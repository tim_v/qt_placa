#include "usersForm.h"
#include <QtSql>
#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QSqlRecord>
#include <QMessageBox>
#include <QDateTime>
#include <QTimer>
#include <QPrinter>
#include <QPainter>
#include <QPrintDialog>
#include <QKeyEvent>

usersForm::usersForm(QSqlDatabase *dbOdStarsa) {
    widget.setupUi(this);
    this->setWindowTitle("Davčni zavezanci");
    // Omogoci gumb za maksimiranje, minimiranje in zaprtje.
    this->setWindowFlags(Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint);
    db = dbOdStarsa;
    QRegExp regExpDavcna("[0-9]{8}");
    QRegExpValidator valDavcna(regExpDavcna, 0);
    widget.lineEditDavcnaStevilka->setValidator(new QRegExpValidator(regExpDavcna, this));
    connect(widget.tableView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(IzbranUporabnik(const QModelIndex &)));
    connect(widget.tableViewLeta, SIGNAL(clicked(const QModelIndex &)), this, SLOT(IzbranoLeto(const QModelIndex &)));
    connect(widget.pushButtonPotrdi, SIGNAL(clicked()), this, SLOT(SprejmiInZapri()));
    connect(widget.pushButtonIzbrisiLeto, SIGNAL(clicked()), this, SLOT(IzbrisiPodatkeZaLeto()));
    connect(widget.pushButtonShrani, SIGNAL(clicked()), this, SLOT(PritisjenShrani()));
    connect(widget.pushButtonIzbrisi, SIGNAL(clicked()), this, SLOT(IzbrisiUporabnika()));
    connect(widget.lineEditIme, SIGNAL(textChanged(const QString &)), this, SLOT(SpremembaPodatkovVLineEdit()));
    connect(widget.lineEditPriimek, SIGNAL(textChanged(const QString &)), this, SLOT(SpremembaPodatkovVLineEdit()));
    connect(widget.lineEditDavcnaStevilka, SIGNAL(textChanged(const QString &)), this, SLOT(SpremembaPodatkovVLineEdit()));
    connect(widget.pushButtonNatisni, SIGNAL(clicked()), this, SLOT(Natisni()));
    GumbiOmogoceni(false);
    PoisciUporabnike();
}

usersForm::~usersForm() {
}

// Poisce uporabnike v bazi.
void usersForm::PoisciUporabnike() {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare("select id, ime, priimek, davcna_stevilka from Zavezanec");
    qry.exec();
    modal->setQuery(qry);
    modal->setHeaderData(1, Qt::Horizontal, tr("Ime"));
    modal->setHeaderData(2, Qt::Horizontal, tr("Priimek"));
    modal->setHeaderData(3, Qt::Horizontal, tr("Davčna številka"));
    // Omogoci filter za razvrsanje po abecedi.
    QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
    m->setDynamicSortFilter(true);
    m->setSourceModel(modal);
    // Vstavi podatke v tabelo.
    widget.tableView->setModel(m);
    widget.tableView->setSortingEnabled(true);
    // Skrije id Zavezanca v GUI.
    widget.tableView->hideColumn(0);
    widget.tableView->setColumnWidth(1,150);
    widget.tableView->setColumnWidth(2,150);
    widget.tableView->setColumnWidth(3,120);
    widget.tableView->setColumnWidth(4,120);
}

// Ugotovi kateri uporabnik je izbran.
void usersForm::IzbranUporabnik(const QModelIndex index) {
    // Ob kliku na uporabnika, resetira izbrano leto na 0.
    izbranoLeto = 0;
    GumbiOmogoceni(false);
    if (index.isValid()) {
        bool ok;
        QString cellText = index.data().toString();
        int vrstica = index.row();
        // Poisce id uporabnika
        idUporabnika = widget.tableView->model()->data(widget.tableView->model()->index(vrstica, 0)).toInt(&ok);
        widget.lineEditIme->setText(widget.tableView->model()->data(widget.tableView->model()->index(vrstica, 1)).toString());
        widget.lineEditPriimek->setText(widget.tableView->model()->data(widget.tableView->model()->index(vrstica, 2)).toString());
        widget.lineEditDavcnaStevilka->setText(widget.tableView->model()->data(widget.tableView->model()->index(vrstica, 3)).toString());
        // Poisce vnesena leta za ustrezni id.
        PoisciLeta(idUporabnika);
    }
    else {
        idUporabnika = 0;
    }
}

// Ugotovi katero leto je izbrano
void usersForm::IzbranoLeto(const QModelIndex index) {
    if (index.isValid()) {
        bool ok;
        int vrstica = index.row();
        QString cellText = index.data().toString();
        // Shrani podatke za izbrano leto
        for (int i = 0; i < 16; i++) {
            podatki[i] = widget.tableViewLeta->model()->data(widget.tableViewLeta->model()->index(vrstica, i+1)).toString();
        }
        izbranoLeto = widget.tableViewLeta->model()->data(widget.tableViewLeta->model()->index(vrstica, 1)).toInt(&ok);
        GumbiOmogoceni(true);
    }
    else {
        izbranoLeto = 0;
        GumbiOmogoceni(false);
        for (int i = 0; i < 16; i++) {
            podatki[i] = "0";
        }
    }
}

// Poisce vnesene leta v bazi za izbranega uporabnika.
void usersForm::PoisciLeta(int id) {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(QStringLiteral("select * from Znesek where zavezanec_id = %1").arg(id));
    qry.exec();
    modal->setQuery(qry);
    // Omogoci filter za razvrsanje po abecedi.
    QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
    m->setDynamicSortFilter(true);
    m->setSourceModel(modal);
    widget.tableViewLeta->setModel(m);
    widget.tableViewLeta->setSortingEnabled(true);
    widget.tableViewLeta->hideColumn(16);
}

// Potrdi formo za prenos podatkov iz te formi v starsevo.
void usersForm::SprejmiInZapri() {
    // Ce podatki niso vneseni, prikazi obvestilo.
    if (izbranoLeto == 0) {
        QMessageBox niPodatkov;
        niPodatkov.setWindowTitle("Napaka");
        niPodatkov.setText("Uporabnikovi podatki niso izbrani.");
        niPodatkov.exec();
    }
    else {
        accept();
        this->close();
    }
}

// Shrani podatke, ce so ustrezni.
void usersForm::PritisjenShrani() {
    // Ce katero polje ni izpoljnjeno prikazi obvestilo o napaki.
    if (ime == "" || priimek == "" || davcna == "") {
        Obvestilo("Ni podatkov", "Uporabnikovi podatki niso ustrezno vnešeni.");
        return;
    }
    QRegExp regExpKontrola("[0-9]{8}");
    QRegExpValidator valKontrola(regExpKontrola, 0);
    int pos = 0;
    // Preveri, ce je davcna pravilne oblike.
    if (valKontrola.validate(davcna, pos) == QValidator::Acceptable) {
        ZapisiPodatke();
    }
    else {
        Obvestilo("Napaka", "Davčna številka je napačna.");
        return;
    }
}

// Preveri, ce so podatki ze vneseni in ponudi vpis.
void usersForm::ZapisiPodatke() {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(QStringLiteral("select * from Zavezanec where davcna_stevilka = '%1'").arg(davcna));
    qry.exec();
    modal->setQuery(qry);
    int zadetki = modal->rowCount();
    // Ce davcna stevilka ze obstaja.
    if (zadetki == 1) {
        QString naslov = "Davcna stevilka že obstaja";
        QString vsebina = QStringLiteral("Uporabnik z davcno stevilko %1 že obstaja. Ali želite prepisati podatke?").arg(davcna);
        if (ObvestiloZIzbiro(naslov, vsebina)) {
            qry.prepare(QStringLiteral("update Zavezanec set ime='%1', priimek='%2' where davcna_stevilka='%3'").arg(ime).arg(priimek).arg(davcna));
            qry.exec();
            Obvestilo("Posodobljeno", "Podatki so uspešno posodobljeni.");
            PoisciUporabnike();
            widget.tableViewLeta->reset();
            return;
        }
    }
    // Ce davcna stevilka se ne obstaja vnesi podatke.
    else {
        if (ObvestiloZIzbiro("Vnos", "Ali ste prepričani, da želite vnesti izbrane podatke?")) {
                qry.prepare(QStringLiteral("INSERT INTO Zavezanec (ime, priimek, davcna_stevilka) "
            "VALUES ('%1', '%2', '%3');").arg(ime).arg(priimek).arg(davcna));
            qry.exec();
            PoisciUporabnike();
            return;
        }
    }    
}

// Ob kliku na gumb za izbris.
void usersForm::IzbrisiUporabnika() {
    if (davcna == "") {
        Obvestilo("Manjkajoč vnos", "Davčna številka ni vnešena.");
        return;
    }
    QRegExp regExpKontrola("[0-9]{8}");
    QRegExpValidator valKontrola(regExpKontrola, 0);
    int pos = 0;
    // Preveri, ce je davcna pravilne oblike.
    if (valKontrola.validate(davcna, pos) != QValidator::Acceptable) {
        Obvestilo("Manjkajoč vnos", "Davčna številka ni pravilne oblike.");
        return;
    }
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(QStringLiteral("select * from Zavezanec where davcna_stevilka = '%1'").arg(davcna));
    qry.exec();
    modal->setQuery(qry);
    int zadetki = modal->rowCount();
    // Ce uporabnik ne obstaja prikaze obvestilo.
    if (zadetki == 0) {
        Obvestilo("Uporabnik ne obstaja", "Uporabnik z izbrano davčno števiko ne obstaja.");
        return;
    }
    QString naslov = "Izbrisi uporabnika";
    QString vsebina = QStringLiteral("Ali ste prepričani, da želite izbrisati uporabnika \"%1 %2\" z davcno stevilko %3?").arg(ime).arg(priimek).arg(davcna);
    if (ObvestiloZIzbiro(naslov, vsebina)) {
        qry.prepare(QStringLiteral("delete from Zavezanec where davcna_stevilka = '%1'").arg(davcna));
        qry.exec();
        PoisciUporabnike();
        return;
    }
}

// Ob kliku na bum za izbris podatkov za dano leto.
void usersForm::IzbrisiPodatkeZaLeto() {
    if (ObvestiloZIzbiro("Izbris podatkov", "Ali ste prepričani, da želite zbrisati podatke za izbrano leto?")) {
        QSqlQuery qry;
        qry.prepare(QStringLiteral("DELETE FROM Znesek WHERE ( leto = %1 AND zavezanec_id = %2 );").arg(izbranoLeto).arg(idUporabnika));
        qry.exec();
        PoisciLeta(idUporabnika);
        GumbiOmogoceni(false);
    }
    else {
        return;
    }
}

// Ob spremembi v lineEdit-ih posodobi vrednosti.
void usersForm::SpremembaPodatkovVLineEdit() {
    ime = widget.lineEditIme->text();
    priimek = widget.lineEditPriimek->text();
    davcna = widget.lineEditDavcnaStevilka->text();
}

// Prikaze enostavno obvestilo.
void usersForm::Obvestilo(QString naslov, QString vsebina) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(naslov);
    msgBox.setText(vsebina);
    msgBox.exec();
}

// Prikaze obvestilo z izbiro da/ne in vrne bool ogovor.
bool usersForm::ObvestiloZIzbiro(QString naslov, QString vsebina) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(naslov);
    msgBox.setText(vsebina);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, "Da");
    msgBox.setButtonText(QMessageBox::No, "Ne");
    msgBox.setDefaultButton(QMessageBox::Yes);
    if (msgBox.exec() == QMessageBox::Yes) {
        return true;
    }
    else {
        return false;
    }
}

// Omogoci ali onemogoci gumbe za tiskanje, izbiro podatkov in izbris leta.
bool usersForm::GumbiOmogoceni(bool omogoceni) {
    if (omogoceni) {
        widget.pushButtonPotrdi->setEnabled(true);
        widget.pushButtonIzbrisiLeto->setEnabled(true);
        widget.pushButtonNatisni->setEnabled(true);
    }
    else {
        widget.pushButtonPotrdi->setEnabled(false);
        widget.pushButtonIzbrisiLeto->setEnabled(false);
        widget.pushButtonNatisni->setEnabled(false);
    }
}

// Natisne uporabnikove podatke za izbrano leto.
bool usersForm::Natisni() {
    QPrinter printer;
    QPrintDialog printDialog(&printer, this);
    // Prikaze okno za tiskanje. Ce je izbrana potrditev, natisne podatke za izbrano leto.
    if (printDialog.exec() == QDialog::Accepted) {
        int leviRob = 80;
        int zgornjiRob = 80;
        int razmikVrstice = 24;
        // Ugotovi trenutni cas.
        QDateTime dt = QDateTime::currentDateTime();
        QString format = "d. MMMM yyyy hh:mm:ss";
        QString cas = dt.toString(format);
//        // Natisne podatke v pdf (3 vrstice kode).
//        printer.setOutputFormat(QPrinter::PdfFormat);
//        QString imeDatoteke = ime + " " + priimek + " " + podatki[0] + ".pdf";
//        printer.setOutputFileName(imeDatoteke);
        QPainter painter;
        // Zacne tiskati.
        painter.begin(&printer);
        painter.setFont(QFont("Times New Roman", 12));
        painter.drawText(leviRob, zgornjiRob, cas);
        zgornjiRob += razmikVrstice;
        painter.drawText(leviRob, zgornjiRob, QStringLiteral("Davčna številka: %1").arg(davcna));
        zgornjiRob += razmikVrstice;
        painter.drawText(leviRob, zgornjiRob, QStringLiteral("Leto obračuna dohodnine: %1").arg(podatki[0]));
        zgornjiRob += razmikVrstice;
        painter.drawText(leviRob, zgornjiRob, QStringLiteral("Ime: %1").arg(ime));
        zgornjiRob += razmikVrstice;
        painter.drawText(leviRob, zgornjiRob, QStringLiteral("Priimek: %1").arg(priimek));
        zgornjiRob += razmikVrstice;
        painter.drawText(leviRob, zgornjiRob, QStringLiteral("Bruto plača: %1 €").arg(PretvoriCentevEvre(podatki[1])));
        zgornjiRob += razmikVrstice;
        painter.drawText(leviRob, zgornjiRob, QStringLiteral("Povračila stroškov: %1 €").arg(PretvoriCentevEvre(podatki[2])));
        zgornjiRob += razmikVrstice;
        // Ce je stevilo otrok vecje od 0, izpise podatke.
        if (podatki[3] != "0") {
            painter.drawText(leviRob, zgornjiRob, QStringLiteral("Število otrok: %1 (število mesecev koriščenja olajšave: %2)")
            .arg(podatki[3]).arg(podatki[4]));
            zgornjiRob += razmikVrstice;
        }
        // Ce je stevilo otrok s posebnimi potrebami vecje od 0, izpise podatke.
        if (podatki[5] != "0") {
            painter.drawText(leviRob, zgornjiRob, QStringLiteral("Število otrok s posebnimi potrebami: %1 (število mesecev koriščenja olajšave: %2)")
            .arg(podatki[5]).arg(podatki[6]));
            zgornjiRob += razmikVrstice;
        }
        // Ce je stevilo drugih vzdrzevanih clanov vecje od 0, izpise podatke.
        if (podatki[7] != "0") {
            painter.drawText(leviRob, zgornjiRob, QStringLiteral("Število drugih vzdrževanih družinskih članov: %1 (število mesecev koriščenja olajšave: %2)")
            .arg(podatki[7]).arg(podatki[8]));
            zgornjiRob += razmikVrstice;
        }
        // V primeru koriščenja olajsave za primer invalidnosti, izpise podatke.
        if (podatki[9] != "0") {
            painter.drawText(leviRob, zgornjiRob, QStringLiteral("Olajšava za primer invalidnosti: DA. Število mesecev koriščenja olajšave: %1").arg(podatki[10]));
            zgornjiRob += razmikVrstice;
        }
        // V primeru koriščenja olajsave za status dijaka ali studenta, izpise podatke.
        if (podatki[11] != "0") {
            painter.drawText(leviRob, zgornjiRob, QStringLiteral("Olajšava za status dijaka ali študenta: DA. Število mesecev koriščenja olajšave: %1").arg(podatki[12]));
            zgornjiRob += razmikVrstice;
        }
        // Ce znasajo druge olajsave vec od 0, izpise podatke.
        if (podatki[13] != "0") {
            painter.drawText(leviRob, zgornjiRob, QStringLiteral("Druge olajšave: %1 € mesečno (število mesecev koriščenja olajšave: %2)")
            .arg(PretvoriCentevEvre(podatki[13])).arg(podatki[14]));
            zgornjiRob += razmikVrstice;
        }
        painter.end();
    }
}

// Vrne znesek z decimalno vejico.
QString usersForm::PretvoriCentevEvre(QString vnos) {
    bool ok;
    unsigned int stevilo = vnos.toInt(&ok);
    QString rezultat;
    if (stevilo < 10) {
        rezultat = QStringLiteral("0,0%1").arg(stevilo);
    }
    else if (stevilo < 100) {
        rezultat = QStringLiteral("0,%1").arg(stevilo);
    }
    else {
        rezultat = QStringLiteral("%1,%2").arg(stevilo / 100).arg(stevilo % 100);
        // Poskrbi, da sta za vejico dve stevili.
        if (rezultat.split(",")[1].size() == 1) {
            rezultat = QStringLiteral("%1,0%2").arg(stevilo / 100).arg(stevilo % 100);
        }
    }
    return rezultat;
}

// Dogodek ob pritisku izbranih tipk.
void usersForm::keyPressEvent(QKeyEvent *event) {
    // Ob pritisku na gumb 'P'.
    if (event->key() == Qt::Key_P) {
        // Ce je hkrati pritisnjen 'Ctrl'.
        if (event->modifiers() == Qt::ControlModifier) {
            // Ce je gumb za tiskanje omogocen, ga aktiviraj.
            if (widget.pushButtonNatisni->isEnabled()) {
                Natisni();
            }
        }
    }
}
