#include "mainForm.h"
#include "settingsForm.h"
#include "usersForm.h"
#include "saveDataForm.h"
#include <QMessageBox>
#include <QtSql>
#include <QCloseEvent>
#include <math.h>
#include <stdbool.h>

mainForm::mainForm() {
    widget.setupUi(this);
    this->setWindowTitle("Izračun dohodnine");
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("placa.sqlite");
    if (!db.open()) {
    }
    PoisciDavcnaObdobja();
    this->comboBoxi = {{
        widget.comboBoxMesecStOtrok,
        widget.comboBoxMesecStOtrokSPotrebami,
        widget.comboBoxMesecDrugiVzdrzevani,
        widget.comboBoxMesecInvalid,
        widget.comboBoxMesecDijakStudent,
        widget.comboBoxMesecDrugeOlajsave
    }};
    // Samo naravna stevila.
    QRegExp regExpNaravnaStevila("^[1-9]{1}\\d{0,6}");
    // Izbere privzeti nacin obracuna. 0 = neto, 1 = bruto, 2 = bruto bruto.
    widget.comboBoxNacinIzracuna->setCurrentIndex(1);
    // Izbere privzeto casovno obdobje obracuna. 0 = mesecno, 1 = letno.
    widget.comboBox_obdobje->setCurrentIndex(1);
    DodeliRegExp();
    connect(widget.pushButtonIzracunaj, SIGNAL(clicked()), this, SLOT(PritisnjenIzracunaj()));
    connect(widget.pushButtonBrisi, SIGNAL(clicked()), this, SLOT(Brisi()));
    connect(widget.comboBox_obdobje, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(PritisnjenIzracunaj()));
    connect(widget.comboBox_obdobje, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(SpremembaObdobja()));
    connect(widget.comboBox_obdobje, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(DodeliRegExp()));
    connect(widget.actionIzhod, SIGNAL(triggered()), this, SLOT(close()));
    connect(widget.actionO_Programu, SIGNAL(triggered()), this, SLOT(PodatkiOProgramu()));
    connect(widget.actionNastavitve, SIGNAL(triggered()), this, SLOT(OdpriNastavitve()));
    connect(widget.actionUporabniki, SIGNAL(triggered()), this, SLOT(OdpriUporabnike()));
    connect(widget.pushButtonShrani, SIGNAL(clicked()), this, SLOT(ShraniPodatke()));
    
}

mainForm::~mainForm() {
}

// Poisce v bazi leta za katera so vnesene davcne stopnje in jih vnese v comboBoxLeto.
void mainForm::PoisciDavcnaObdobja() {
    bool ok;
    widget.comboBoxLeto->clear();
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare("select leto from Obdavcitev");
    qry.exec();
    modal->setQuery(qry);
    // Sesteje st. vnesenih let v bazi.
    int stLet = modal->rowCount();
    // Ce v bazi ni podatkov, prikazi obvestilo.
    if (stLet == 0) {
        NapakaVBazi();
    }
    int vsaLeta[stLet];
    // Vnese leta iz baze v polje vsaLeta.
    for (int i = 0; i < stLet; i++) {
        vsaLeta[i] = modal->record(i).value(0).toInt(&ok);
    }
    // Razvrsti leta.
    std::sort(vsaLeta, vsaLeta+stLet);
    // Vnese vsa leta v comboBoxLeto.
    for (int i = 0; i < stLet; i++) {
        widget.comboBoxLeto->addItem(QStringLiteral("%1").arg(vsaLeta[i]));
    }
}

// Obvestilo, ce v bazi ni vneseno nobeno leto.
void mainForm::NapakaVBazi() {
    Obvestilo("Napaka", "V podatkovni bazi ni ustreznih podatkov.", "Razumem");
}

// V bazi poisce ustrezne davcne stopnje za izbrano leto.
void mainForm::PoisciDavcneStopnje() {
    // Ugotovi izbrano leto.
    izbranoLeto = widget.comboBoxLeto->itemText(widget.comboBoxLeto->currentIndex());
    stopnjaDelojemalecCent = PoisciStopnjoZaLeto("stopnja_delojemalec", izbranoLeto);
    stopnjaDelodajalecCent = PoisciStopnjoZaLeto("stopnja_delodajalec", izbranoLeto);
    dohodninskaLestvica[0][0] = 0;
    dohodninskaLestvica[0][1] = PoisciStopnjoZaLeto("stopnja_1_do", izbranoLeto);
    dohodninskaLestvica[0][2] = PoisciStopnjoZaLeto("stopnja_1_davek", izbranoLeto);
    dohodninskaLestvica[0][3] = PoisciStopnjoZaLeto("stopnja_1_stopnja", izbranoLeto);
    dohodninskaLestvica[0][4] = PoisciStopnjoZaLeto("stopnja_1_izplacilo", izbranoLeto);
    dohodninskaLestvica[1][0] = dohodninskaLestvica[0][1];
    dohodninskaLestvica[1][1] = PoisciStopnjoZaLeto("stopnja_2_do", izbranoLeto);
    dohodninskaLestvica[1][2] = PoisciStopnjoZaLeto("stopnja_2_davek", izbranoLeto);
    dohodninskaLestvica[1][3] = PoisciStopnjoZaLeto("stopnja_2_stopnja", izbranoLeto);
    dohodninskaLestvica[1][4] = PoisciStopnjoZaLeto("stopnja_2_izplacilo", izbranoLeto);
    dohodninskaLestvica[2][0] = dohodninskaLestvica[1][1];
    dohodninskaLestvica[2][1] = PoisciStopnjoZaLeto("stopnja_3_do", izbranoLeto);
    dohodninskaLestvica[2][2] = PoisciStopnjoZaLeto("stopnja_3_davek", izbranoLeto);
    dohodninskaLestvica[2][3] = PoisciStopnjoZaLeto("stopnja_3_stopnja", izbranoLeto);
    dohodninskaLestvica[2][4] = PoisciStopnjoZaLeto("stopnja_3_izplacilo", izbranoLeto); 
    dohodninskaLestvica[3][0] = dohodninskaLestvica[2][1];
    dohodninskaLestvica[3][1] = PoisciStopnjoZaLeto("stopnja_4_do", izbranoLeto);
    dohodninskaLestvica[3][2] = PoisciStopnjoZaLeto("stopnja_4_davek", izbranoLeto);
    dohodninskaLestvica[3][3] = PoisciStopnjoZaLeto("stopnja_4_stopnja", izbranoLeto);
    dohodninskaLestvica[3][4] = PoisciStopnjoZaLeto("stopnja_4_izplacilo", izbranoLeto);
    dohodninskaLestvica[4][0] = dohodninskaLestvica[3][1];
    dohodninskaLestvica[4][1] = PoisciStopnjoZaLeto("stopnja_5_do", izbranoLeto);
    dohodninskaLestvica[4][2] = PoisciStopnjoZaLeto("stopnja_5_davek", izbranoLeto);
    dohodninskaLestvica[4][3] = PoisciStopnjoZaLeto("stopnja_5_stopnja", izbranoLeto);
    dohodninskaLestvica[4][4] = PoisciStopnjoZaLeto("stopnja_5_izplacilo", izbranoLeto);
    splosnaOlajsava[0][0] = 0;
    splosnaOlajsava[0][1] = PoisciStopnjoZaLeto("splosna_olajsava_nizja_do", izbranoLeto);
    splosnaOlajsava[0][2] = PoisciStopnjoZaLeto("splosna_olajsava_nizja_znesek", izbranoLeto);
    splosnaOlajsava[0][3] = 0;
    splosnaOlajsava[0][4] = 0;
    splosnaOlajsava[1][0] = splosnaOlajsava[0][1];
    splosnaOlajsava[1][1] = PoisciStopnjoZaLeto("splosna_olajsava_visja_od", izbranoLeto);
    splosnaOlajsava[1][2] = PoisciStopnjoZaLeto("splosna_olajsava_visja_znesek", izbranoLeto);
    splosnaOlajsava[1][3] = PoisciStopnjoZaLeto("splosna_olajsava_srednja_osnova_za_pribitek", izbranoLeto);
    splosnaOlajsava[1][4] = PoisciStopnjoZaLeto("splosna_olajsava_srednja_faktor_zmanjsanja", izbranoLeto);
    splosnaOlajsava[2][0] = splosnaOlajsava[1][1];
    splosnaOlajsava[2][1] = -1;
    splosnaOlajsava[2][2] = splosnaOlajsava[1][2];
    splosnaOlajsava[2][3] = 0;
    splosnaOlajsava[2][4] = 0;
    olajsaveZaOtroke[0] = PoisciStopnjoZaLeto("olajsave_za_otroke_1", izbranoLeto);
    olajsaveZaOtroke[1] = PoisciStopnjoZaLeto("olajsave_za_otroke_2", izbranoLeto);
    olajsaveZaOtroke[2] = PoisciStopnjoZaLeto("olajsave_za_otroke_3", izbranoLeto);
    olajsaveZaOtroke[3] = PoisciStopnjoZaLeto("olajsave_za_otroke_4", izbranoLeto);
    olajsaveZaOtroke[4] = PoisciStopnjoZaLeto("olajsave_za_otroke_5", izbranoLeto);
    povecanjeZaVsakegaDodatnega = PoisciStopnjoZaLeto("olajsava_za_otroke_dodatni", izbranoLeto);
    olajsaveZaOtrokeSPosebnimi[0] = PoisciStopnjoZaLeto("olajsave_za_OSPP_1", izbranoLeto);
    olajsaveZaOtrokeSPosebnimi[1] = PoisciStopnjoZaLeto("olajsave_za_OSPP_2", izbranoLeto);
    olajsaveZaOtrokeSPosebnimi[2] = PoisciStopnjoZaLeto("olajsave_za_OSPP_3", izbranoLeto);
    olajsaveZaOtrokeSPosebnimi[3] = PoisciStopnjoZaLeto("olajsave_za_OSPP_4", izbranoLeto);
    olajsaveZaOtrokeSPosebnimi[4] = PoisciStopnjoZaLeto("olajsave_za_OSPP_5", izbranoLeto);
    povecanjeZaVsakegaDodatnegaSPosebnimiPotrebami = PoisciStopnjoZaLeto("olajsava_za_OSPP_dodatni", izbranoLeto);
    OlajsavaDrugiVzdrzevani = PoisciStopnjoZaLeto("olajsava_drugi_vzdrzevani", izbranoLeto);
    olajsavaZaInvalide = PoisciStopnjoZaLeto("olasava_za_invalide", izbranoLeto);
    olajsavaZaDijakeInStudente = PoisciStopnjoZaLeto("olajsava_dijak_student", izbranoLeto);
}

// Poisce posamezno stopnjo pri zeljenem letu.
unsigned int mainForm::PoisciStopnjoZaLeto(QString kljuc, QString leto) {
    bool ok;
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(QStringLiteral("select %1 from Obdavcitev where leto = %2").arg(kljuc).arg(leto));
    qry.exec();
    modal->setQuery(qry);
    unsigned int stevilo = modal->record(0).value(0).toInt(&ok);
    return stevilo;
}

// Za mnozenje pozitivnih int. Vrne po potrebi za izbran faktor pomanjsano stevilo.
unsigned int mainForm::Pomnozi(unsigned int faktor1, unsigned int faktor2, short odrezi) {
    unsigned long long longFaktor1 = faktor1;
    unsigned long long longFaktor2 = faktor2;
    unsigned long long longProdukt = longFaktor1 * longFaktor2;
    unsigned int produkt;
    // Testiraj ce ni natanco, naredi funckijo v tem primeru.
    unsigned long long delitelj = pow(10, odrezi);
    // Ce je stevilo preveliko, vrni 0.
    if ((longProdukt / delitelj) >= 4294967295) {
        return 0;
    }
    else {
        produkt = (unsigned int) (longProdukt / delitelj);
    }
    // Ce je ostanek >= 0.5 pristej 1 produktu. Ce je rezultat preveliko stevilo, vrne 0.
    if (((longProdukt % delitelj) * 2) >= delitelj) {
        return (produkt + 1);
    }
    else {
        return produkt;
    }
}

// Deli 2 stevili. Vrne po potrebi za izbran faktor povecano stevilo.
unsigned int mainForm::Deli(unsigned int deljenec, unsigned int delitelj, short povecaj) {
    if (delitelj == 0) {
        return 0;
    }
    unsigned long long longDeljenec = deljenec;
    unsigned long long longDelitelj = delitelj;
    unsigned long long longFaktor = pow(10, povecaj);
    unsigned long long longKolicnik = longDeljenec * longFaktor / longDelitelj;
    unsigned long long ostanek = longDeljenec * longFaktor % longDelitelj;
    // Zaokrozi ostanek navzgor, ce je potrebno.
    if (ostanek * 2 >= delitelj) {
        longKolicnik += 1;
    }
    if (longKolicnik > 4294967295) {
        longKolicnik = 0;
    }
    return (int) longKolicnik;
}

// Za mnozenje long long. Lahko racuna z negativnimi stevili.
long long mainForm::PomnoziLong(long long faktor1, long long faktor2) {
    long long longProdukt = faktor1 * faktor2;
    return longProdukt;
}

// Za deljenje long long. Lahko racuna z negativnimi stevili. Za razliko od funkcije "Deli" ne omogoca izbire st. decimalnih mest.
long long mainForm::DeliLong(long long deljenec, long long delitelj) {
    if (delitelj == 0) {
        return 0;
    }
    long long longKolicnik = deljenec / delitelj;
    long long ostanek = deljenec % delitelj;
    // Zaokrozi ostanek, ce je potrebno.
    if (ostanek * 2 >= delitelj) {
        longKolicnik += 1;
    }
    return longKolicnik;
}

// Ob pritisku na gumb 'Izracunaj' izracuna dohodnino, prispevke itd.
void mainForm::PritisnjenIzracunaj() {
    PoisciDavcneStopnje();
    povraciloStroskovCent = PretvoriQStringVCente(widget.lineEditPovracila->text());
    short indeks = widget.comboBoxNacinIzracuna->currentIndex();
    bool letniObracun = widget.comboBox_obdobje->currentIndex();
    unsigned int letnaVrednost = PretvoriQStringVCente(widget.lineEditZnesekZaIzracun->text());
    // Ce je izbrano mesecno obdobje izracuna.
    if (!letniObracun) {
        letnaVrednost = Pomnozi(letnaVrednost, 12, 0);
        povraciloStroskovCent = Pomnozi(povraciloStroskovCent, 12, 0);
    }
    // Ce je izbrano "Neto".
    if (indeks == 0) {
        netoCent = letnaVrednost;
        izplaciloCent = netoCent + povraciloStroskovCent;
        nesplosneOlajsaveCent = IzracunajNesplosneOlajsave(letniObracun);
        IzpeljiBruto();
        PrispevkiDelodajalca();
        prispevkiDelojemalcaCent = PrispevkiDelojemalca(brutoCent);
        IzracunajVseOlajsave(letniObracun);
        IzracunajDohodninskoOsnovo();
        dohodninaCent = IzracunajDohodnino(dohodninskaOsnovaCent);
        skupniStrosekCent = brutoBrutoCent + povraciloStroskovCent;
    }
    // Ce je izbrano "Bruto" ali "Bruto bruto".
    else {
        if (indeks == 1) {
            brutoCent = letnaVrednost;
        }
        if (indeks == 2) {
            brutoBrutoCent = letnaVrednost;
        }
        PrispevkiDelodajalca();
        prispevkiDelojemalcaCent = PrispevkiDelojemalca(brutoCent);
        IzracunajVseOlajsave(letniObracun);
        IzracunajDohodninskoOsnovo();
        dohodninaCent = IzracunajDohodnino(dohodninskaOsnovaCent);
        netoCent = brutoCent - dohodninaCent - prispevkiDelojemalcaCent;
        izplaciloCent = netoCent + povraciloStroskovCent;
        skupniStrosekCent = brutoBrutoCent + povraciloStroskovCent;
    }
    widget.lineEditIzplacilo->setText(VrednostGledeNaObdobje(izplaciloCent));
    widget.lineEditNeto->setText(VrednostGledeNaObdobje(netoCent));
    widget.lineEditBruto->setText(VrednostGledeNaObdobje(brutoCent));
    widget.lineEditBrutoBruto->setText(VrednostGledeNaObdobje(brutoBrutoCent));
    widget.lineEditPrispevkiDelodajalca->setText(VrednostGledeNaObdobje(prispevkiDelodajalcaCent));
    widget.lineEditPrispevkiDelojemalca->setText(VrednostGledeNaObdobje(prispevkiDelojemalcaCent));
    widget.lineEditSkupniStrosek->setText(VrednostGledeNaObdobje(skupniStrosekCent));
    widget.lineEditDohodnina->setText(VrednostGledeNaObdobje(dohodninaCent));
    IzpisiStopnjo();
    NastaviBarve();
}

// Izracuna vrednost prispevkov delojemalca.
unsigned int mainForm::PrispevkiDelojemalca(unsigned int bruto) {
    return Pomnozi(bruto, stopnjaDelojemalecCent, 4);
}

// Ce je znana bruto vrednost, lahko izracuna bruto bruto in obratno. Izracuna tudi prispevke delodajalca.
void mainForm::PrispevkiDelodajalca() {
    short indeks = widget.comboBoxNacinIzracuna->currentIndex();
    // Ce je izbrano "Neto" ali "Bruto".
    if (indeks == 0 || indeks == 1) {
        prispevkiDelodajalcaCent = Pomnozi(brutoCent, stopnjaDelodajalecCent, 4);
        brutoBrutoCent = brutoCent + prispevkiDelodajalcaCent;
        // Natisne vrednost prispevkov.
        widget.lineEditPrispevkiDelodajalca->setText(PretvoriCenteVQString(prispevkiDelodajalcaCent));
    }
    // Ce je izbrano "Bruto bruto".
    if (indeks == 2) {
        // Pristeje 10.000 kar je enakovredno 100%.
        brutoCent = Deli(brutoBrutoCent, (stopnjaDelodajalecCent + 10000), 4);
        prispevkiDelodajalcaCent = brutoBrutoCent - brutoCent;
    }
}

// Zbrise vnosno polje in ponastavi vrednosti olajsav.
void mainForm::Brisi() {
    widget.lineEditZnesekZaIzracun->setText("");
    widget.lineEditPovracila->setText("");
    widget.spinBoxStOtrok->setValue(0);
    widget.spinBoxStOtrokSPosebnimiPotrebami->setValue(0);
    widget.spinBoxDrugiVzdrzevani->setValue(0);
    widget.checkBoxInvalid->setChecked(false);
    widget.checkBoxDijakStudent->setChecked(false);
    widget.lineEditDrugeOlajsave->setText("");
    widget.lineEditNeto->setText("0,00");
    widget.lineEditBruto->setText("0,00");
    widget.lineEditBrutoBruto->setText("0,00");
    widget.lineEditSkupniStrosek->setText("0,00");
    widget.lineEditIzplacilo->setText("0,00");
    widget.lineEditDohodnina->setText("0,00");
    widget.lineEditDohodninskaOsnova->setText("0,00");
    widget.lineEditPrispevkiDelodajalca->setText("0,00");
    widget.lineEditPrispevkiDelojemalca->setText("0,00");
    widget.lineEditSplosnaOlajsava->setText("0,00");
    widget.lineEditOlajsava->setText("0,00");
    widget.comboBoxMesecStOtrok->setCurrentIndex(12);
    widget.comboBoxMesecStOtrokSPotrebami->setCurrentIndex(12);
    widget.comboBoxMesecDrugiVzdrzevani->setCurrentIndex(12);
    widget.comboBoxMesecInvalid->setCurrentIndex(12);
    widget.comboBoxMesecDijakStudent->setCurrentIndex(12);
    widget.comboBoxMesecDrugeOlajsave->setCurrentIndex(12);
    widget.labelIzracunanDavek->setText("0%");
    // Naredi ozadje prosojno.
    widget.frameBarvaObdavcitev->setStyleSheet("background-color: rgba(0, 0, 0, 0);");
}

// Pretvori QString v lineEditZnesekZaIzracun v int. Vrne vneseno vrednost v centih.
unsigned int mainForm::PretvoriQStringVCente(QString vnos) {
    bool ok;
    int skupnoCenti;
    // Izracunaj vneseno vrednost v centih.
    if (vnos.contains(",")) {
        int evriVCente = 100 * vnos.split(",")[0].toInt(&ok);
        int centi;
        // Ugotovi koliko decimalnih mest je za vejico.
        int decimalnaMesta = vnos.split(",")[1].size();
        // Spremenljivki centi dodeli ustrezno vrednost.
        switch (decimalnaMesta) {
            case 1: {
                centi = 10 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 2: {
                centi = vnos.split(",")[1].toInt(&ok);
                break;
            }
            default: {
                centi = 0;
            }
        }
        skupnoCenti = evriVCente + centi;
    } else {
        skupnoCenti = 100 * vnos.toInt(&ok);
    }
    return skupnoCenti;
}

// Prevori int v QString. Vrednost izrazena v stotinkah.
QString mainForm::PretvoriCenteVQString(unsigned int stevilo) {
    QString rezultat;
    if (stevilo < 10) {
        rezultat = QStringLiteral("0,0%1").arg(stevilo);
    }
    else if (stevilo < 100) {
        rezultat = QStringLiteral("0,%1").arg(stevilo);
    }
    else {
        rezultat = QStringLiteral("%1,%2").arg(stevilo / 100).arg(stevilo % 100);
        // Poskrbi, da sta za vejico dve stevili.
        if (rezultat.split(",")[1].size() == 1) {
            rezultat = QStringLiteral("%1,0%2").arg(stevilo / 100).arg(stevilo % 100);
        }
    }
    return rezultat;
}

// V primeru izbranega mesecnega obracune vrne dvanajstino letne vrednosti.
QString mainForm::VrednostGledeNaObdobje(unsigned int vrednost) {
    if (widget.comboBox_obdobje->currentIndex() == 1){
        return PretvoriCenteVQString(vrednost);
    }
    unsigned int mesecno = Deli(vrednost, 12 , 0);
    
    return PretvoriCenteVQString(mesecno);
}

// Ugotovi dohodninsko osnovo.
void mainForm::IzracunajDohodninskoOsnovo() {
    if (vsotaOlajsavCent >= (brutoCent - prispevkiDelojemalcaCent)) {
        dohodninskaOsnovaCent = 0;
    }
    else {
        dohodninskaOsnovaCent = brutoCent - vsotaOlajsavCent - prispevkiDelojemalcaCent;
    }
    widget.lineEditDohodninskaOsnova->setText(VrednostGledeNaObdobje(dohodninskaOsnovaCent));
}

// Izracuna dohodnino.
unsigned int mainForm::IzracunajDohodnino(unsigned int osnova) {
    unsigned int dohodnina = 0;
    for (int i = 0; i < 5; i++) {
        // Poisce v kateri dohodninski razred sodi osnova.
        if (osnova <= dohodninskaLestvica[i][1]) {
            unsigned int kolicinaVNajvisjemDosezenemRazredu = osnova - dohodninskaLestvica[i][0];
            unsigned int davekVNajvisjemDosezenemRazredu = Pomnozi(kolicinaVNajvisjemDosezenemRazredu, dohodninskaLestvica[i][3], 4);
            dohodnina = davekVNajvisjemDosezenemRazredu + dohodninskaLestvica[i][2];
            break;
        }
    }
    return dohodnina;
}

// Vrne neto znesek.
unsigned int mainForm::IzracunajNeto(unsigned int bruto, unsigned int dohodnina) {
    unsigned int neto;
    unsigned int prispevki = PrispevkiDelojemalca(bruto);
    neto = bruto - prispevki - dohodnina;
    return neto;
}

// Sesteje vrednost vseh olajsav.
void mainForm::IzracunajVseOlajsave(bool letniObracun) {
    nesplosneOlajsaveCent = IzracunajNesplosneOlajsave(letniObracun);
    unsigned int splosnaOlajsava = IzracunajSplosnoOlajsavo(brutoCent);
    vsotaOlajsavCent = nesplosneOlajsaveCent + splosnaOlajsava;
    widget.lineEditOlajsava->setText(VrednostGledeNaObdobje(vsotaOlajsavCent));
    widget.lineEditSplosnaOlajsava->setText(VrednostGledeNaObdobje(splosnaOlajsava));
}

// Vrne vrednost spošne olajsave.
unsigned int mainForm::IzracunajSplosnoOlajsavo(unsigned int bruto) {
    unsigned int olajsava;
    for (int i = 0; i < 3; i++) {
        if (bruto >= splosnaOlajsava[i][0] && bruto <= splosnaOlajsava[i][1]) {
            olajsava = splosnaOlajsava[i][2] + (splosnaOlajsava[i][3] - Pomnozi(splosnaOlajsava[i][4], bruto, 6));
            break;
        }
    }
    return olajsava;
}

// Izracuna vrednost vseh olajsav, razen splosne.
unsigned int mainForm::IzracunajNesplosneOlajsave(bool letniObracun) {
    unsigned int vsota = 0;
    unsigned short stOtrok = widget.spinBoxStOtrok->value();
    unsigned short stOtrokSPosebnimiPotrebami = widget.spinBoxStOtrokSPosebnimiPotrebami->value();
    unsigned short stDrugihVzdrzevanih = widget.spinBoxDrugiVzdrzevani->value();
    // Ce je stevilo otrok vecje od 0, pristej olajsavo za slednje k vosti.
    if (stOtrok > 0) {
        unsigned int letnaOlajsavaZaOtroke = IzracunajOlajsavoZaOtroke(olajsaveZaOtroke, 5, povecanjeZaVsakegaDodatnega, stOtrok);
        // Ce je letni obracun, preveri koliko mesecev se koristi olajsava.
        if (letniObracun) {
            short stMesecevOlajsaveZaOtroke = widget.comboBoxMesecStOtrok->currentIndex();
            letnaOlajsavaZaOtroke = IzracunDelneOlajsave(letnaOlajsavaZaOtroke, stMesecevOlajsaveZaOtroke);
       }
        vsota += letnaOlajsavaZaOtroke;
    }
    // Ce je stevilo otrok vecje od 0, pristej olajsavo za slednje k vsoti.
    if (stOtrokSPosebnimiPotrebami > 0) {
        unsigned int letnaOlajsavaZaOtrokeSPP = IzracunajOlajsavoZaOtroke(olajsaveZaOtrokeSPosebnimi, 5, povecanjeZaVsakegaDodatnegaSPosebnimiPotrebami, stOtrokSPosebnimiPotrebami);        
        // Ce je letni obracun, preveri koliko mesecev se koristi olajsava.
        if (letniObracun) {
            short stMesecevOlajsaveZaOtrokeSPP = widget.comboBoxMesecStOtrokSPotrebami->currentIndex();
            letnaOlajsavaZaOtrokeSPP = IzracunDelneOlajsave(letnaOlajsavaZaOtrokeSPP, stMesecevOlajsaveZaOtrokeSPP);
        }
        vsota += letnaOlajsavaZaOtrokeSPP;
    }
    // Ce je stevilo drugov vzdrezavnih vecje od 0, pristej olajsavo za slednje k vsoti.
    if (stDrugihVzdrzevanih > 0) {
        unsigned int letnaOlajsavaDrugi = stDrugihVzdrzevanih * OlajsavaDrugiVzdrzevani;
        if (letniObracun) {
            short stMesecevZaDruge = widget.comboBoxMesecDrugiVzdrzevani->currentIndex();
            letnaOlajsavaDrugi = IzracunDelneOlajsave(letnaOlajsavaDrugi, stMesecevZaDruge);
        }
        vsota += letnaOlajsavaDrugi;
    }    
    if (widget.checkBoxInvalid->isChecked()) {
        unsigned int olajsavaIn = olajsavaZaInvalide;
        if (letniObracun) {
            short stMesecevInvalid = widget.comboBoxMesecInvalid->currentIndex();
            olajsavaIn = IzracunDelneOlajsave(olajsavaIn, stMesecevInvalid);
        }
        vsota += olajsavaIn;
    }
    if (widget.checkBoxDijakStudent->isChecked()) {
        unsigned int olajsavaDijakStudent = olajsavaZaDijakeInStudente;
        if (letniObracun) {
            short stMesecevDijakStudent = widget.comboBoxMesecDijakStudent->currentIndex();
            olajsavaDijakStudent = IzracunDelneOlajsave(olajsavaDijakStudent, stMesecevDijakStudent);
        }
        vsota += olajsavaDijakStudent;
    }
    unsigned int drugeOlajsave = PretvoriQStringVCente(widget.lineEditDrugeOlajsave->text());
    if (letniObracun) {
        short stMesecevDrugeOlajsave = widget.comboBoxMesecDrugeOlajsave->currentIndex();
        drugeOlajsave = IzracunDelneOlajsave(drugeOlajsave, stMesecevDrugeOlajsave);
    }
    vsota += drugeOlajsave;
    return vsota;
}

// Vrne vrednost olajsave za otroke/-s posebnimi potrebami.
unsigned int mainForm::IzracunajOlajsavoZaOtroke(unsigned int lestvica[], unsigned short stElementov, unsigned int pribitek, unsigned short stOtrok) {
    unsigned int sestevekOlajsav = 0;
    unsigned int zadnjaOlajsava = 0;
    for (int i = 1; i <= stOtrok; i++) {
        if (i < stElementov) {
            zadnjaOlajsava = lestvica[i - 1];
            sestevekOlajsav += zadnjaOlajsava;
        }
        else {
            zadnjaOlajsava += pribitek;
            sestevekOlajsav += zadnjaOlajsava;
        }
    }
    return sestevekOlajsav;
}

// Izracuna vrednost olajsave, odvisno od tega, koliko mesecev se uposteva.
unsigned int mainForm::IzracunDelneOlajsave(unsigned int olajsava, short stMesecev) {
    return Deli(Pomnozi(olajsava, stMesecev, 0), 12, 0);
}

// Ob spremembi obdobja izracuna, ce je izbrano letno, omogoci izbiro stevila mesecev koriscenja posamezne olajsave.
void mainForm::SpremembaObdobja() {
    if (widget.comboBox_obdobje->currentIndex() == 0) {
        for (int i = 0; i < 6; i++) {
            // Onemogoci polja za izbiro mesecev koriscenja olajsav in gumb za shranjevanje.
            comboBoxi[i]->setEnabled(false);
            widget.pushButtonShrani->setEnabled(false);
        }
    }
    if (widget.comboBox_obdobje->currentIndex() == 1) {
        for (int i = 0; i < 6; i++) {
            // Omogoci polja za izbiro mesecev koriscenja olajsav in gumb za shranjevanje.
            comboBoxi[i]->setEnabled(true);
            widget.pushButtonShrani->setEnabled(true);
        }
    }
}

// Izracuna efektivno stopnjo obdavcitve.
void mainForm::IzracunajEfektivnoStopnjo() {
    efektivnaStopnja = 10000 - Deli(izplaciloCent, skupniStrosekCent, 4);
}

// Izpise efektivno obdavcitev kot odstotek.
void mainForm::IzpisiStopnjo() {
    QString stopnja;
    IzracunajEfektivnoStopnjo();
    stopnja = PretvoriCenteVQString(efektivnaStopnja) + "%";
    widget.labelIzracunanDavek->setText(stopnja);
}

// Izpelje dohodnino.
unsigned int mainForm::IzpeljiDohodnino(unsigned int znesek) {
    unsigned int dohodnina;
    short v; // vrstica/dacna stopnja v katero sodi znesek
    for (int i = 0; i < 5; i++) {
        // Ce je doticna davcna stopnja navzgor omejena, preveri, ce je ustrezna.
        if (dohodninskaLestvica[i][1] != -1) {
            // Ce je znesek </= razlike zgornje stopnje lestvice in dohodnine, je znesek v tem razredu.
            if (znesek <= (dohodninskaLestvica[i][1] - dohodninskaLestvica[i+1][2])) {
                v = i;
                break;
            }
        }
        // Ce je doticna davcna stopnja najvisja mozna (oz. je -1).
        else {
            v = i;
            break;
        }
    }
    unsigned int netoIzRazreda = znesek - dohodninskaLestvica[v][0] + dohodninskaLestvica[v][2];
    unsigned int dohOsnovaVTemRazredu = Deli(netoIzRazreda, (10000 - dohodninskaLestvica[v][3]), 4);
    dohodnina = Pomnozi(dohOsnovaVTemRazredu, dohodninskaLestvica[v][3], 4) + dohodninskaLestvica[v][2];
    return dohodnina;
}

// Iz neto vrednosti izpelje bruto vrednost.
void mainForm::IzpeljiBruto() {
    // Ce obstaja meja za najvecno davcno olajsavo, ki je vecja od 0 (leta 2019 da, leta 2020 pa te meje ni vec).
    if (splosnaOlajsava[0][1] > 0) {
        // Ce je neto manjsi/enak kot znasa najvecji neto znesek, kateri se lahko vodi do najvecje spl. olajsave.
        if (netoCent <= Pomnozi(splosnaOlajsava[0][1], (10000 - stopnjaDelojemalecCent), 4)) {
            if (PreveriIzracunPriZnaniSploOlajsavi(splosnaOlajsava[0][2])) {
                IzpeljiBrutoPriZnaniSplOlajsavi(splosnaOlajsava[0][2]);
                return;
            }
        }
    }
    // Preveri, ce se izracun izide pri najnizji splosni olajsavi.
    if (PreveriIzracunPriZnaniSploOlajsavi(splosnaOlajsava[2][2])) {
        IzpeljiBrutoPriZnaniSplOlajsavi(splosnaOlajsava[2][2]);
        return;
    }
    // Ce ni niti najnizja, niti najvisja, mora biti splosna olajsava nekje vmes.
    else {
        IzpeljiBrutoInSplosnoOlajsavo();
    }
}

// Preveri, ce se izracun pri dani splosni olajsavi izzide.
bool mainForm::PreveriIzracunPriZnaniSploOlajsavi(unsigned int splosnaO) {
    unsigned int znesek;
    // Ce je vrednost olajsav vecja/enaka od neto. Potem ni dohodnine.
    if ((splosnaO + nesplosneOlajsaveCent) >= netoCent) {
        znesek = 0;
    }
    else {
        znesek = netoCent - splosnaO - nesplosneOlajsaveCent;
    }
    unsigned int dohodnina = IzpeljiDohodnino(znesek);
    // Izracuna potencialne vrednosti.
    unsigned int potBrutoCent = Deli((dohodnina + netoCent), (10000 - stopnjaDelojemalecCent), 4);
    unsigned int potSplOlajsava = IzracunajSplosnoOlajsavo(potBrutoCent);
    // Ostopanje do 10 centov smatra za OK.
    if (((potSplOlajsava + 10) > splosnaO) && ((potSplOlajsava - 10) < splosnaO)) {
        return 1;
    }
    else {
        return 0;
    }
}

// Izracuna bruto pri znani splosni olajsavi
void mainForm::IzpeljiBrutoPriZnaniSplOlajsavi(unsigned int splosnaO) {
    // Ce je vrednost olajsav vecja/enaka od neto. Potem ni dohodnine.
    if ((splosnaO + nesplosneOlajsaveCent) >= netoCent) {
        brutoCent = Deli(netoCent, (10000 - stopnjaDelojemalecCent), 4);
        return;
    }
    else {
        // "znesek" je neto zmanjsan za vse olajsave. Ta znesek je kjucen za izepljavo dohodnine.
        unsigned int znesek = netoCent - splosnaO - nesplosneOlajsaveCent;
        unsigned int dohodnina = IzpeljiDohodnino(znesek);
        brutoCent = Deli((dohodnina + netoCent), (10000 - stopnjaDelojemalecCent), 4);
    }
}

// Kadar so olajsave vec od neto ni dohodnine, izpelje bruto.
void mainForm::IzpeljiBrutoKoOlajsaveVecOdNeto() {
    brutoCent = Deli(netoCent, (10000 - stopnjaDelojemalecCent), 4);
}

// Izracuna bruto, kadar davcna stopnja ni znana.
void mainForm::IzpeljiBrutoInSplosnoOlajsavo() {
    // Deljenec na 10 decimalk
    long long deljenec = (long long)dohodninskaLestvica[0][3]
                         * (long long)(nesplosneOlajsaveCent + splosnaOlajsava[1][2] + splosnaOlajsava[1][3]) * 10000
                         - (long long)netoCent * 100000000;
    // Delitelj na 10 decimalk
    long long delitelj = (long long)dohodninskaLestvica[0][3]
                         * (long long)(splosnaOlajsava[1][4] + (10000 - stopnjaDelojemalecCent) * 100)
                         - (long long)(10000 - stopnjaDelojemalecCent) * 1000000;
    // Pretvori natancnost delitelja na 8 decimalk.
    delitelj = DeliLong(delitelj, 100);
    // Deli 10 z 8 decimalnim mestom, da nastane rezultat v stotinah.
    int potBrutoCent = DeliLong(deljenec, delitelj);
    // Ce je vrednost negativna ali nic, se izracun ne izide. Olajsave morajo biti vec/enake kot neto.
    if (potBrutoCent <= 0) {
        IzpeljiBrutoKoOlajsaveVecOdNeto();
        return;
    }
    unsigned int potSplOlajsava = IzracunajSplosnoOlajsavo(potBrutoCent);
    // Preveri, ce se se izracun izide.
    if (PreveriIzracunPriZnaniSploOlajsavi(potSplOlajsava)) {
        IzpeljiBrutoPriZnaniSplOlajsavi(potSplOlajsava);
        return;
    }
    // Ce se racun ne izide je vrednost olajsav vecja od neto. Dohodnine potem ni.
    else {
        IzpeljiBrutoKoOlajsaveVecOdNeto();
        return;
    }
}

// Prikaze okno z podatki o programu.
void mainForm::PodatkiOProgramu() {
    Obvestilo("Aplikacija za izračun dohodnine",
    "Aplikacija omogoča izračun dohodnine, shranjevanje in urejanje podatkov uporabnikov ter tiskanje.");
}

// Odpre okno za potrditev izhoda.
void mainForm::closeEvent(QCloseEvent *event) {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Izhod");
    msgBox.setText("Želite zapreti program?");
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, "Da");
    msgBox.setButtonText(QMessageBox::No, "Ne");
    msgBox.setDefaultButton(QMessageBox::Yes);
    // Koda, da se pred izhodom pojavi okno za potrditev.
//    if(msgBox.exec() == QMessageBox::Yes){
//        this->close();
//    }
//    else {
//        event->ignore();
//    }
    // Koda za običajni izhod iz aplikacije.
    this->close();
}

// Odpre okno z nastavitvami.
void mainForm::OdpriNastavitve() {
    if (db.isOpen()) {
        settingsForm *SF = new settingsForm(&db);
        SF->setModal(true);
        SF->exec();
        PoisciDavcnaObdobja();
    }
}

// Odpre okno z uporabnikovimi podatki.
void mainForm::OdpriUporabnike() {
    if (db.isOpen()) {
        usersForm *UF = new usersForm(&db);
        UF->setModal(true);
        UF->exec();
        // Ce je bil pritisnjen gumb za uvoz podatkov v formi userForm.
        if (UF->result() == QDialog::Accepted) {
            VnesiUporabnikovePodatke(UF->podatki);
        }
    }
}

// Vpise uporabnikove podatke v lineEdit-e.
void mainForm::VnesiUporabnikovePodatke(QString podatki[]) {
    bool ok;
    PoisciDavcnaObdobja();
    bool letoVneseno = false;
    int stLet = widget.comboBoxLeto->count();
    for (int i = 0; i < stLet; i++) {
        if (podatki[0] == widget.comboBoxLeto->itemText(i)) {
            letoVneseno = true;
            widget.comboBoxLeto->setCurrentIndex(i);
            break;
        }
    }
    // Ce leto za izbrane podatke ni v bazi, prikaze obvestilo.
    if (!letoVneseno) {
        QMessageBox niLeta;
        niLeta.setWindowTitle("Manjkajoči podatki");
        niLeta.setText(QStringLiteral("Davčne stopnje za leto %1 niso vnešene.").arg(podatki[0]));
        niLeta.setStandardButtons(QMessageBox::Yes);
        niLeta.setButtonText(QMessageBox::Yes, "Nadaljuj");
        niLeta.setDefaultButton(QMessageBox::Yes);
        if (niLeta.exec() == QMessageBox::Yes) {
            return;
        }
    }
    // Vnese podatke v polja in naredi izracune.
    widget.comboBox_obdobje->setCurrentIndex(1);
    widget.comboBoxNacinIzracuna->setCurrentIndex(1);
    widget.lineEditZnesekZaIzracun->setText(OblikujQStringZaVnos(podatki[1]));
    widget.lineEditPovracila->setText(OblikujQStringZaVnos(podatki[2]));
    widget.spinBoxStOtrok->setValue(podatki[3].toInt(&ok));
    widget.comboBoxMesecStOtrok->setCurrentIndex(podatki[4].toInt(&ok));
    widget.spinBoxStOtrokSPosebnimiPotrebami->setValue(podatki[5].toInt(&ok));
    widget.comboBoxMesecStOtrokSPotrebami->setCurrentIndex(podatki[6].toInt(&ok));
    widget.spinBoxDrugiVzdrzevani->setValue(podatki[7].toInt(&ok));
    widget.comboBoxMesecDrugiVzdrzevani->setCurrentIndex(podatki[8].toInt(&ok));
    if (podatki[9].toInt(&ok)) {
        widget.checkBoxInvalid->setChecked(true);
    }
    else {
        widget.checkBoxInvalid->setChecked(false);
    }
    widget.comboBoxMesecInvalid->setCurrentIndex(podatki[10].toInt(&ok));
    if (podatki[11].toInt(&ok)) {
        widget.checkBoxDijakStudent->setChecked(true);
    }
    else {
        widget.checkBoxDijakStudent->setChecked(false);
    }
    widget.comboBoxMesecDijakStudent->setCurrentIndex(podatki[12].toInt(&ok));
    widget.lineEditDrugeOlajsave->setText(OblikujQStringZaVnos(podatki[13]));
    widget.comboBoxMesecDrugeOlajsave->setCurrentIndex(podatki[14].toInt(&ok));
    PritisnjenIzracunaj();
}

// Odpre formo za shranjevanje podatkov.
void mainForm::ShraniPodatke() {
    if (db.isOpen()) {
        ZapisiVrednosti();
        unsigned int *vrednosti;
        vrednosti = podatki;
        saveDataForm *SDF = new saveDataForm(&db, izbranoLeto, vrednosti);
        SDF->setModal(true);
        SDF->exec();
    }
}

// Zapise vnesene vrednosti za prenos v saveDataForm.
void mainForm::ZapisiVrednosti() {
    // Prikaze obvestilo, če je izbran mesecni obracun.
    if (widget.comboBox_obdobje->currentIndex() == 0) {    
        Obvestilo("Napaka", "Program shranjuje samo podatke za letni obračun dohodnine.");
    }
    bool ok;
    PritisnjenIzracunaj();
    podatki[0] = PretvoriQStringVCente(widget.lineEditBruto->text()); // v bazi: bruto INTEGER
    podatki[1] = PretvoriQStringVCente(widget.lineEditPovracila->text()); // v bazi: povracila_stroskov INTEGER
    podatki[2] = widget.spinBoxStOtrok->text().toInt(&ok); // v bazi: st_otrok INTEGER
    if (podatki[2] != 0) {
        podatki[3] = widget.comboBoxMesecStOtrok->currentText().toInt(&ok); // v bazi: st_otrok_meseci INTEGER
    }
    // Ce je stevilo otrok 0, shrani stevilo mesecev tudi kot 0.
    else {
        podatki[3] = 0;
    }
    podatki[4] = widget.spinBoxStOtrokSPosebnimiPotrebami->text().toInt(&ok); // v bazi: st_otrok_SPP INTEGER
    if (podatki[4] == 0) {
        podatki[5] = 0;
    }
    // Ce je stevilo otrok s posebnimi potrebami 0, shrani stevilo mesecev tudi kot 0.
    else {
        podatki[5] = widget.comboBoxMesecStOtrokSPotrebami->currentText().toInt(&ok); // v bazi: st_otrok_SPP_meseci INTEGER
    }
    podatki[6] = widget.spinBoxDrugiVzdrzevani->text().toInt(&ok); // v bazi: drugi_vzdrzevani_clani INTEGER
    if (podatki[6] == 0) {
        podatki[7] = 0;
    }
    // Ce je stevilo drugih vzdrzevanih oseb 0, shrani stevilo mesecev tudi kot 0.
    else {
        podatki[7] = widget.comboBoxMesecDrugiVzdrzevani->currentText().toInt(&ok); // v bazi: drugi_vzdrzevani_clani_meseci INTEGER
    }
    if (widget.checkBoxInvalid->isChecked()) {
        podatki[8] = 1; // v bazi: invalid INTEGER
        podatki[9] = widget.comboBoxMesecInvalid->currentText().toInt(&ok); // v bazi: invalid_meseci INTEGER
    }
    else {
        podatki[8] = 0;
        podatki[9] = 0;
    }
    if (widget.checkBoxDijakStudent->isChecked()) {
        podatki[10] = 1; // v bazi: dijak_student INTEGER
        podatki[11] = widget.comboBoxMesecDijakStudent->currentText().toInt(&ok);  // v bazi: dijak_student_meseci INTEGER
    }
    else {
        podatki[10] = 0;
        podatki[11] = 0;
    }
    podatki[12] = widget.lineEditDrugeOlajsave->text().toInt(&ok); // v bazi: druge_olajsave INTEGER
    if (podatki[12] != 0) {
        podatki[13] = widget.comboBoxMesecDrugeOlajsave->currentText().toInt(&ok); // v bazi: druge_olajsave_meseci INTEGER
    } 
    else {
        podatki[13] = 0;
    }
}

// Prikaze enostavno obvestilo.
void mainForm::Obvestilo(QString naslov, QString vsebina, QString gumb) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(naslov);
    msgBox.setText(vsebina);
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.setButtonText(QMessageBox::Yes, gumb);
    msgBox.exec();
}

// QString v vrednosti centov spremeni v euro z decimalno vejico.
QString mainForm::OblikujQStringZaVnos(QString centi) {
    bool ok;
    unsigned int centiUnsInt = centi.toInt(&ok);
    QString euro = PretvoriCenteVQString(centiUnsInt);
    return euro;
}

// Nastavi barvo glede na efektivno obdavcitev.
void mainForm::NastaviBarve() {
    float rdecaOzadje;
    float zelenaOzadje;
    // Pri stopnji do 20% je ozadje zeleno.
    if (efektivnaStopnja < 2000) {
        rdecaOzadje = 0;
        zelenaOzadje = 255;
    }
    // Pri stopnji med 20% in 40% je RGB ozadje 255 zeleno in narascajoce rdece.
    else if (efektivnaStopnja < 4000) {
        zelenaOzadje = 255;
        rdecaOzadje = 255 * ((efektivnaStopnja - 2000) / 2000.0);
    }
    // Pri stopnji med 40% in 60% je RGB zadje 255 rdeče in padajoce zeleno.
    else if (efektivnaStopnja < 6000) {
        rdecaOzadje = 255;
        zelenaOzadje = (255 * (1 - (efektivnaStopnja - 4000) / 2000.0));
    }
    // Ce je obdavcitev >= 60% je ozadje rdece.
    else {
        rdecaOzadje = 255;
        zelenaOzadje = 0;
    }
    widget.frameBarvaObdavcitev->setStyleSheet(QStringLiteral("background:rgb(%1,%2,0);").arg(rdecaOzadje).arg(zelenaOzadje));
}

// Ob pritisku na razne tipke naredi razno.
void mainForm::keyPressEvent(QKeyEvent *event) {
    // Ob pritisku katerekoli tipke Enter izracuna podatke.
    if ((event->key() == Qt::Key_Return) || (event->key() == Qt::Key_Enter)) {
        PritisnjenIzracunaj();
    }
    // Ctrl + N izbere neto izracun.
    if ((event->key() == Qt::Key_N)) {
        widget.comboBoxNacinIzracuna->setCurrentIndex(0);
    }
    // Ctrl + B izbere bruto izracun.
    if ((event->key() == Qt::Key_B)) {
        widget.comboBoxNacinIzracuna->setCurrentIndex(1);
    }
}

// Doloco koliko mestno stevilo se lahko vnese.
void mainForm::DodeliRegExp() {
    // Stevila od 0 do 9.999.999,99.
    QRegExp regExpPlaca("^[1-9]{1}\\d{0,6}(,\\d{0,2}){0,1}$|^0{1},[0-9]{0,2}");
    // Stevila od 0 do 999.999,99.
    QRegExp regExpPlacaMesec("^[1-9]{1}\\d{0,5}(,\\d{0,2}){0,1}$|^0{1},[0-9]{0,2}");
    // Ce je izbran letni obracun.
    if (widget.comboBox_obdobje->currentIndex() == 1) {
        widget.lineEditZnesekZaIzracun->setValidator(new QRegExpValidator(regExpPlaca, this));
        widget.lineEditPovracila->setValidator(new QRegExpValidator(regExpPlaca, this));
        widget.lineEditDrugeOlajsave->setValidator(new QRegExpValidator(regExpPlaca, this));
    // Ce je izbran mesecni obracun.
    }
    if (widget.comboBox_obdobje->currentIndex() == 0) {
        widget.lineEditZnesekZaIzracun->setValidator(new QRegExpValidator(regExpPlacaMesec, this));
        widget.lineEditPovracila->setValidator(new QRegExpValidator(regExpPlacaMesec, this));
        widget.lineEditDrugeOlajsave->setValidator(new QRegExpValidator(regExpPlacaMesec, this));
    }
}
