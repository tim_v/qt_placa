#include "saveDataForm.h"
#include <QtSql>
#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QMessageBox>

saveDataForm::saveDataForm(QSqlDatabase *dbOdStarsa, QString leto, unsigned int *vnos) {
    widget.setupUi(this);
    // Omogoci gumba za minimiranje in zaprtje.
    this->setWindowFlags(Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint);
    this->setWindowTitle("Shrani vnešene podatke");
    this->leto = leto;
    podatki = vnos;
    db = dbOdStarsa;
    connect(widget.tableView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(IzbranUporabnik(const QModelIndex &)));
    connect(widget.pushButtonShrani, SIGNAL(clicked()), this, SLOT(PritisnjenShrani()));
    PoisciUporabnike();
}

saveDataForm::~saveDataForm() {
}

// Poisce uporabnike v bazi.
void saveDataForm::PoisciUporabnike() {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare("select id, ime, priimek, davcna_stevilka from Zavezanec");
    qry.exec();
    modal->setQuery(qry);
    modal->setHeaderData(1, Qt::Horizontal, tr("Ime"));
    modal->setHeaderData(2, Qt::Horizontal, tr("Priimek"));
    modal->setHeaderData(3, Qt::Horizontal, tr("Davčna številka"));
    // Omogoci filter za razvrsanje po abecedi.
    QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
    m->setDynamicSortFilter(true);
    m->setSourceModel(modal);
    // Vstavi podatke v tabelo.
    widget.tableView->setModel(m);
    widget.tableView->setSortingEnabled(true);
    // Skrije id Zavezanca v GUI.
    widget.tableView->hideColumn(0);
    widget.tableView->setColumnWidth(1,150);
    widget.tableView->setColumnWidth(2,150);
    widget.tableView->setColumnWidth(3,120);
    widget.tableView->setColumnWidth(4,120);
}

// Ugotovi kateri uporabnik je izbran.
void saveDataForm::IzbranUporabnik(const QModelIndex index) {
    if (index.isValid()) {
        bool ok;
        QString cellText = index.data().toString();
        int vrstica = index.row();
        // Poisce id uporabnika
        idUporabnika = widget.tableView->model()->data(widget.tableView->model()->index(vrstica, 0)).toInt(&ok);
    }
    else {
        idUporabnika = 0;
    }
}

// Ugotovi, ce je izbran znesek za leto ze vnesen. Ce je, vrne id zneska.
int saveDataForm::PoisciAliZnesekZeObstaja() {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(QStringLiteral("SELECT * FROM Znesek WHERE (zavezanec_id = \"%1\" AND leto = \"%2\");").arg(idUporabnika).arg(leto));
    qry.exec();
    modal->setQuery(qry);
    // Sesteje st. vnesenih let v bazi.
    int stLet = modal->rowCount();
    if (stLet == 0) {
        return 0;
    }
    else {
        bool ok;
        return modal->record(0).value(0).toInt(&ok);
    }
}

// Prikaze enostavno obvestilo.
void saveDataForm::Obvestilo(QString naslov, QString vsebina, QString gumb) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(naslov);
    msgBox.setText(vsebina);
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.setButtonText(QMessageBox::Yes, gumb);
    msgBox.exec();
}

// Prikaze obvestilo z izbiro da/ne in vrne bool ogovor.
bool saveDataForm::ObvestiloZIzbiro(QString naslov, QString vsebina) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(naslov);
    msgBox.setText(vsebina);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, "Da");
    msgBox.setButtonText(QMessageBox::No, "Ne");
    msgBox.setDefaultButton(QMessageBox::Yes);
    if (msgBox.exec() == QMessageBox::Yes) {
        return true;
    }
    else {
        return false;
    }
}

// Ob pritisku na shrani preveri zacne postopek za shranjevanje podatkov.
void saveDataForm::PritisnjenShrani() {
    if (idUporabnika == 0) {
        Obvestilo("Manjkajo podatki", "Izbran ni noben uporabnik.");
        return;
    }
    int idZneska = PoisciAliZnesekZeObstaja();
    if (idZneska != 0) {
        if (ObvestiloZIzbiro("Leto že obstaja.", "Podatki za izbrano leto so že vnešeni. Jih želite prepisati?")) {
            PrepisiPodatke(idZneska);
            this->close();
        }
    }
    else {
        VnesiPodatke();
        this->close();
    }
}

// Vnese novo leto v bazo.
void saveDataForm::VnesiPodatke() {
    ZdruziVrednostiZaNovVnos();
    QSqlQuery qry;
    qry.prepare("INSERT INTO Znesek (leto, bruto, povracila_stroskov, st_otrok, st_otrok_meseci, st_otrok_SPP, "
    "st_otrok_SPP_meseci, drugi_vzdrzevani_clani, drugi_vzdrzevani_clani_meseci, invalid, "
    "invalid_meseci, dijak_student, dijak_student_meseci, druge_olajsave, druge_olajsave_meseci, zavezanec_id) "
    "VALUES (" + vrednostiVQString + ")");
    qry.exec();
    Obvestilo("Podatki vnešeni", QStringLiteral("Uporabnikovi podatki za leto %1 so bili uspešno vnešeni.").arg(leto), "Zapri");
}

// Posodobi podatke v bazi.
void saveDataForm::PrepisiPodatke(int idZneska) {
    QSqlQuery qry;
    qry.prepare(QStringLiteral("UPDATE Znesek set "
    "bruto = \"%1\", "
    "povracila_stroskov = \"%2\", "
    "st_otrok = \"%3\", "
    "st_otrok_meseci = \"%4\", "
    "st_otrok_SPP = \"%5\", "
    "st_otrok_SPP_meseci = \"%6\", "
    "drugi_vzdrzevani_clani = \"%7\", "
    "drugi_vzdrzevani_clani_meseci = \"%8\", "
    "invalid = \"%9\", "
    "invalid_meseci = \"%10\", "
    "dijak_student = \"%11\", "
    "dijak_student_meseci = \"%12\", "
    "druge_olajsave = \"%13\", "
    "druge_olajsave_meseci = \"%14\" "
    "WHERE id = \"%15\";")
    .arg(QString::number(podatki[0]))
    .arg(QString::number(podatki[1]))
    .arg(QString::number(podatki[2]))
    .arg(QString::number(podatki[3]))
    .arg(QString::number(podatki[4]))
    .arg(QString::number(podatki[5]))
    .arg(QString::number(podatki[6]))
    .arg(QString::number(podatki[7]))
    .arg(QString::number(podatki[8]))
    .arg(QString::number(podatki[9]))
    .arg(QString::number(podatki[10]))
    .arg(QString::number(podatki[11]))
    .arg(QString::number(podatki[12]))
    .arg(QString::number(podatki[13]))
    .arg(idZneska));
    qry.exec();
    Obvestilo("Podatki posodobljeni", "Podatki so bili uspešno posodobljeni.");
}

// Oblikuje vrednostiVQString za novi vnos podatkov v bazo.
void saveDataForm::ZdruziVrednostiZaNovVnos() {
    vrednostiVQString = leto + ", ";
        for (int i = 0; i < 14; i++) {
        if (i != 13) {
            vrednostiVQString += QStringLiteral("%1, ").arg(podatki[i]);
        }
        else {
            vrednostiVQString += QStringLiteral("%1").arg(podatki[13]);
        }
    }
    vrednostiVQString += ", " + QStringLiteral("%1").arg(idUporabnika);
}
