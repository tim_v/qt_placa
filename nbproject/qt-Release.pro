# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Release/MinGW-Windows
TARGET = qt_placa
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += release 
PKGCONFIG +=
QT = core gui widgets printsupport sql
SOURCES += main.cpp mainForm.cpp saveDataForm.cpp saveForm.cpp settingsForm.cpp usersForm.cpp
HEADERS += mainForm.h saveDataForm.h saveForm.h settingsForm.h usersForm.h
FORMS += mainForm.ui saveDataForm.ui saveForm.ui settingsForm.ui usersForm.ui
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/MinGW-Windows
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += 
equals(QT_MAJOR_VERSION, 4) {
QMAKE_CXXFLAGS += -std=c++11
}
equals(QT_MAJOR_VERSION, 5) {
CONFIG += c++11
}
RC_ICONS = myappico.ico
