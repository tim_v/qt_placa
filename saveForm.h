#ifndef _SAVEFORM_H
#define _SAVEFORM_H

#include "ui_saveForm.h"
#include <QtSql>

class saveForm : public QDialog {
    Q_OBJECT
public:
    saveForm(QSqlDatabase *, QString , QString leto = "prazno");
    virtual ~saveForm();
private:
    Ui::saveForm widget;
    QString vnos;
    QString izbranoLeto;
    QString stolpciIzBaze = "leto,"
    "stopnja_delojemalec,"
    "stopnja_delodajalec,"
    "stopnja_1_do,"
    "stopnja_1_davek,"
    "stopnja_1_stopnja,"
    "stopnja_1_izplacilo,"
    "stopnja_2_do,"
    "stopnja_2_davek,"
    "stopnja_2_stopnja,"
    "stopnja_2_izplacilo,"
    "stopnja_3_do,"
    "stopnja_3_davek,"
    "stopnja_3_stopnja,"
    "stopnja_3_izplacilo,"
    "stopnja_4_do,"
    "stopnja_4_davek,"
    "stopnja_4_stopnja,"
    "stopnja_4_izplacilo,"
    "stopnja_5_do,"
    "stopnja_5_davek,"
    "stopnja_5_stopnja,"
    "stopnja_5_izplacilo,"
    "splosna_olajsava_nizja_do,"
    "splosna_olajsava_nizja_znesek,"
    "splosna_olajsava_srednja_osnova_za_pribitek,"
    "splosna_olajsava_srednja_faktor_zmanjsanja,"
    "splosna_olajsava_visja_od,"
    "splosna_olajsava_visja_znesek,"
    "olajsave_za_otroke_1,"
    "olajsave_za_otroke_2,"
    "olajsave_za_otroke_3,"
    "olajsave_za_otroke_4,"
    "olajsave_za_otroke_5,"
    "olajsava_za_otroke_dodatni,"
    "olajsave_za_OSPP_1,"
    "olajsave_za_OSPP_2,"
    "olajsave_za_OSPP_3,"
    "olajsave_za_OSPP_4,"
    "olajsave_za_OSPP_5,"
    "olajsava_za_OSPP_dodatni,"
    "olajsava_drugi_vzdrzevani,"
    "olasava_za_invalide,"
    "olajsava_dijak_student";
private slots:
    void PritisnjenShrani();
    void ZapisiPodatkeVBazo(bool);
    void UgotoviIzbranoLeto();
    bool PreveriCeJeLetoZeVneseno();
    void PrikaziObvestiloZaShranjevanje();
    void PrikaziObvestilo(bool);
};

#endif /* _SAVEFORM_H */
