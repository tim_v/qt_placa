#ifndef _MAINFORM_H
#define _MAINFORM_H

#include "ui_mainForm.h"
#include <QtSql>

class mainForm : public QMainWindow {
    Q_OBJECT
public:
    mainForm();
    virtual ~mainForm();
private:
    Ui::mainForm widget;
    QSqlDatabase db;
    QString izbranoLeto;
    // Vrednosti v centih. Na primer 123,45€ je izrazeno kot 12345.
    unsigned int netoCent, brutoCent, brutoBrutoCent, prispevkiDelojemalcaCent, prispevkiDelodajalcaCent,
                 dohodninskaOsnovaCent, dohodninaCent, splosnaOlajsavaCent, nesplosneOlajsaveCent,
                 vsotaOlajsavCent, povraciloStroskovCent, izplaciloCent, skupniStrosekCent;
    // Vrednosti v stokratniku odstotka. Na primer 1610 = 16,10%.
    unsigned int stopnjaDelojemalecCent, stopnjaDelodajalecCent, efektivnaStopnja;
    /* Dohodninska lestvica. Vsebuje cente in stokratnike odstotka.
     * Podaki v obliki: nad | do | davek do te stopnje | stopnja (odstotek) | izplacilo do vkljucno tega razreda
     * Primer za leto 2019:
     * unsigned int dohodninskaLestvica[5][5] = {
     *    {0, 802134, 0, 1600, 673793},
     *    {802134, 2040000, 128341, 2700, 1577435},
     *    {2040000, 4800000, 462565, 3400, 3399035},
     *    {4800000, 7090720, 1400965, 3900, 4796374},
     *    {7090720, -1, 2294346, 5000, -1}
     * }; */
    unsigned int dohodninskaLestvica[5][5];
    /*unsigned int splosnaOlajsava[3][5] = {
     *    // nad | do | splosna olajsava | osnova za izracun pribitka | faktor zmanjsanja v 10^5.
     *    {0, 1116637, 651982, 0, 0},
     *    {1116637, 1331683, 330270, 1992215, 1496010},
     *    {1331683, -1, 330270, 0, 0}
     *};
    */
    unsigned int splosnaOlajsava[3][5];
    std::array<QComboBox*, 6> comboBoxi;
    // Spremenljivke z vnesenimi vrednostmi v centih.
    unsigned int olajsaveZaOtroke[5]; //= {243692, 264924, 441854, 618785, 795714};
    unsigned int povecanjeZaVsakegaDodatnega; //= 176930;
    unsigned int olajsaveZaOtrokeSPosebnimi[5]; //= {883000, 904232, 1081162, 1258093, 1435022};
    unsigned int povecanjeZaVsakegaDodatnegaSPosebnimiPotrebami; //= 176930;
    unsigned int OlajsavaDrugiVzdrzevani; //= 243692;
    unsigned int olajsavaZaInvalide; //= 1765884;
    unsigned int olajsavaZaDijakeInStudente; //= 330270;
    unsigned int podatki[14];
private slots:
    void PoisciDavcnaObdobja();
    void NapakaVBazi();
    void PoisciDavcneStopnje();
    unsigned int PoisciStopnjoZaLeto(QString, QString);
    unsigned int Pomnozi(unsigned int, unsigned int, short);
    unsigned int Deli(unsigned int, unsigned int, short);
    long long PomnoziLong(long long, long long);
    long long DeliLong(long long, long long);
    void PritisnjenIzracunaj();
    unsigned int PrispevkiDelojemalca(unsigned int);
    void PrispevkiDelodajalca();
    void Brisi();
    unsigned int PretvoriQStringVCente(QString);
    QString PretvoriCenteVQString(unsigned int);
    QString VrednostGledeNaObdobje(unsigned int);
    void IzracunajDohodninskoOsnovo();
    unsigned int IzracunajDohodnino(unsigned int);
    unsigned int IzracunajNeto(unsigned int, unsigned int);
    void IzracunajVseOlajsave(bool);
    unsigned int IzracunajSplosnoOlajsavo(unsigned int);
    unsigned int IzracunajNesplosneOlajsave(bool);
    unsigned int IzracunajOlajsavoZaOtroke(unsigned int[], unsigned short, unsigned int, unsigned short);
    unsigned int IzracunDelneOlajsave(unsigned int, short);
    void SpremembaObdobja();
    void IzracunajEfektivnoStopnjo();
    void IzpisiStopnjo();
    unsigned int IzpeljiDohodnino(unsigned int);
    void IzpeljiBruto();
    bool PreveriIzracunPriZnaniSploOlajsavi(unsigned int splosnaO);
    void IzpeljiBrutoPriZnaniSplOlajsavi(unsigned int splosnaO);
    void IzpeljiBrutoKoOlajsaveVecOdNeto();
    void IzpeljiBrutoInSplosnoOlajsavo();
    void PodatkiOProgramu();
    void closeEvent(QCloseEvent*);
    void OdpriNastavitve();
    void OdpriUporabnike();
    void VnesiUporabnikovePodatke(QString [15]);
    void ShraniPodatke();
    void ZapisiVrednosti();
    void Obvestilo(QString naslov, QString vsebina, QString gumb = "OK");
    QString OblikujQStringZaVnos(QString);
    void NastaviBarve();
    void keyPressEvent(QKeyEvent *);
    void DodeliRegExp();
};

#endif /* _MAINFORM_H */
