#ifndef _USERSFORM_H
#define _USERSFORM_H

#include "ui_usersForm.h"
#include <QtSql>

class usersForm : public QDialog {
    Q_OBJECT
public:
    usersForm(QSqlDatabase *);
    virtual ~usersForm();
    QString podatki[16];
private:
    Ui::usersForm widget;
    QSqlDatabase *db;
    QString ime;
    QString priimek;
    QString davcna;
    int idUporabnika = 0;
    int izbranoLeto = 0;
private slots:
    void PoisciUporabnike();
    void IzbranUporabnik(const QModelIndex);
    void IzbranoLeto(const QModelIndex);
    void PoisciLeta(int id_uporabnika);
    void SprejmiInZapri();
    void PritisjenShrani();
    void ZapisiPodatke();
    void IzbrisiUporabnika();
    void IzbrisiPodatkeZaLeto();
    void SpremembaPodatkovVLineEdit();
    void Obvestilo(QString, QString);
    bool ObvestiloZIzbiro(QString, QString);
    bool GumbiOmogoceni(bool omogoceni);
    bool Natisni();
    QString PretvoriCentevEvre(QString);
    void keyPressEvent(QKeyEvent *);
};

#endif /* _USERSFORM_H */
