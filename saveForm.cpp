#include "saveForm.h"
#include <QtSql>
#include <QMessageBox>

saveForm::saveForm(QSqlDatabase *db, QString vnos, QString leto) {
    widget.setupUi(this);
    this->setWindowTitle("Shrani podatke za vnešeno leto");
    this->setWindowFlags(Qt::WindowCloseButtonHint);
    this->vnos = vnos;
    QRegExp regExpPlaca("^[1-9]{1}\\d{0,3}");
    widget.lineEdit->setValidator(new QRegExpValidator(regExpPlaca, this));
    if (leto != "prazno") {
        widget.lineEdit->setText(leto);
        izbranoLeto = leto;
    }
    connect(widget.pushButtonPreklici, SIGNAL(clicked()), this, SLOT(close()));
    connect(widget.lineEdit, SIGNAL(textChanged(const QString &)), this, SLOT(UgotoviIzbranoLeto()));
    connect(widget.pushButtonShrani, SIGNAL(clicked()), this, SLOT(PritisnjenShrani()));
}

saveForm::~saveForm() {
}

// Ob pritisku na gumb za shranjevanje odpre ustrezno okno.
void saveForm::PritisnjenShrani() {
    if (widget.lineEdit->text().isEmpty()) {
        PrikaziObvestilo(false);
    }
    // Ce je v lineEdit vneseno leto, odpre obvestilo za shranjevanje.
    else {
        PrikaziObvestiloZaShranjevanje();
    }
}

// Izvede zapis v bazo.
void saveForm::ZapisiPodatkeVBazo(bool prepis) {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    if (prepis) {
        qry.prepare("select id from Obdavcitev where leto = " + izbranoLeto);
        qry.exec();
        modal->setQuery(qry);
        // Ugotovi primarni kljuc za izbrano leto v bazi.
        QString id = modal->record(0).value(0).toString();
        QStringList stolpci = stolpciIzBaze.split(",");
        QStringList vrednosti = vnos.split(",");
        QString posodobitve;
        // Ustreznemu stoplcu doda ustrezno vrednost za del priprave za qry.
        for (int i = 0; i < 43; i++) {
            posodobitve += " " + stolpci[i+1] + "=" + vrednosti[i];
            // Ce ni zadnji par, dodaj vejico.
            if (i != 42) {
                posodobitve += ",";
            }
            // Zadnjemu paru dodaj samo presledek.
            else {
                posodobitve += " ";
            }
        }
        qry.prepare("update Obdavcitev set" + posodobitve + " where id=" + id);
        qry.exec();
    }
    if (!prepis) {
        qry.prepare("insert into Obdavcitev (" + stolpciIzBaze + ") VALUES (" + izbranoLeto + "," + vnos + ");");
        qry.exec();
    }
}

// Ugotovi v lineEdit vneseno stevilo.
void saveForm::UgotoviIzbranoLeto() {
    izbranoLeto = widget.lineEdit->text();
}

// Preveri, ce je leto ze vneseno v bazo.
bool saveForm::PreveriCeJeLetoZeVneseno() {
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare("select leto from Obdavcitev");
    qry.exec();
    modal->setQuery(qry);
    // Sesteje st. vnesenih let v bazi.
    int stLetVBazi = modal->rowCount();
    for (int i = 0; i < stLetVBazi; i++) {
        if (izbranoLeto == modal->record(i).value(0)) {
            return true;
        }
    }
    return false;
}

// Vprasa, ce zelimo dokoncno vnesti podatke.
void saveForm::PrikaziObvestiloZaShranjevanje() {
    bool prepis = PreveriCeJeLetoZeVneseno();
    QMessageBox potrdiVnos;
    potrdiVnos.setWindowTitle("Vnos v bazo");
    if (prepis) {
        potrdiVnos.setText(QStringLiteral("Ali ste prepričani, da želite posodobiti podatke za leto %1?").arg(izbranoLeto));
    }
    else {
        potrdiVnos.setText(QStringLiteral("Ali ste prepričani, da želite vnesti v bazo podatke za leto %1?").arg(izbranoLeto));
    }
    potrdiVnos.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    potrdiVnos.setButtonText(QMessageBox::Yes, "Da");
    potrdiVnos.setButtonText(QMessageBox::No, "Ne");
    if (potrdiVnos.exec() == QMessageBox::Yes) {
        // zapisi podatke
        ZapisiPodatkeVBazo(prepis);
        PrikaziObvestilo(true);
        this->close();
    }
    else {
        return;
    }
}

// Prikaze obvestilo za uspesno shranjevanje, ce true ali pa nevneseno leto, ce false.
void saveForm::PrikaziObvestilo(bool uspeh) {
    QMessageBox obvestilo;
    if (uspeh) {
        obvestilo.setWindowTitle("Shranjevanje uspešno");
        obvestilo.setText("Leto je bilo uspešno shranjeno.");
    }
    else {
        obvestilo.setWindowTitle("Napaka");
        obvestilo.setText("Vnešeno ni nobeno leto.");
    }
    obvestilo.setStandardButtons(QMessageBox::Yes);
    obvestilo.setButtonText(QMessageBox::Yes, "Razumem");
    obvestilo.exec();
}
