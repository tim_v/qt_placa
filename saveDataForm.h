#ifndef _SAVEDATAFORM_H
#define _SAVEDATAFORM_H

#include "ui_saveDataForm.h"
#include <QtSql>

class saveDataForm : public QDialog {
    Q_OBJECT
public:
    saveDataForm(QSqlDatabase *, QString, unsigned int *);
    virtual ~saveDataForm();
private:
    Ui::saveDataForm widget;
    QSqlDatabase *db;
    QString leto;
    unsigned int *podatki;
    int idUporabnika = 0;
    QString vrednostiVQString;
private slots:
    void PoisciUporabnike();
    void IzbranUporabnik(const QModelIndex);
    int PoisciAliZnesekZeObstaja();
    void Obvestilo(QString naslov, QString vsebina, QString gumb = "OK");
    bool ObvestiloZIzbiro(QString naslov, QString vsebina);
    void PritisnjenShrani();
    void VnesiPodatke();
    void PrepisiPodatke(int);
    void ZdruziVrednostiZaNovVnos();
};

#endif /* _SAVEDATAFORM_H */
