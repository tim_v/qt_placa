#include "settingsForm.h"
#include "saveForm.h"
#include <QtSql>
#include <QMessageBox>

settingsForm::settingsForm(QSqlDatabase *dbOdStarsa) {
    widget.setupUi(this);
    this->setWindowTitle("Nastavitve");
    // Omogoci gumb za maksimiranje, minimiranje in zaprtje.
    this->setWindowFlags(Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint|Qt::WindowMinimizeButtonHint);
    DefinirajPodatkovneBoxe();
    ValidirajVnos();
    db = dbOdStarsa;
    PoisciVnesenaDavcnaObdobja();
    PoisciVneseneDavcneStopnje();
    connect(widget.pushButtonIzbrisiLeto, SIGNAL(clicked()), this, SLOT(IzbrisiLeto()));
    connect(widget.comboBoxLeto, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(PoisciVneseneDavcneStopnje()));
    connect(widget.lineEditDo1, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditDo2, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditDo3, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditDo4, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditStopnja1, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditStopnja2, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditStopnja3, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditStopnja4, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditOlajsavaDo1, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditOlajsavaOd3, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.lineEditOlajsavaZnesek3, SIGNAL(textChanged(const QString &)), this, SLOT(PosodobiVrednostiVOnemogoceniLineEdit()));
    connect(widget.pushButtonShraniPodatkeVNovoLeto, SIGNAL(clicked()), this, SLOT(ShraniPodatkeVNovoLeto()));
    connect(widget.pushButtonPosodobiPodatke, SIGNAL(clicked()), this, SLOT(PosodobiPodatke()));
}

settingsForm::~settingsForm() {
}

// Izbrise leto, ce le-to ni 2019.
void settingsForm::IzbrisiLeto() {
    // Prikaze obvestilo, kadar je v bazi samo eno vneseno leto, da ga ni mogoce izbrisati.
    if (stLetVBazi <= 1) {
        QMessageBox zavrnjenaZahteva;
        zavrnjenaZahteva.setWindowTitle("Zavrnjena zahteva");
        zavrnjenaZahteva.setText("Izbris zavrnjen. Ostati mora vsaj eno vnešeno leto.");
        zavrnjenaZahteva.setStandardButtons(QMessageBox::Yes);
        zavrnjenaZahteva.setButtonText(QMessageBox::Yes, "Nadaljuj");
        zavrnjenaZahteva.setDefaultButton(QMessageBox::Yes);
        if (zavrnjenaZahteva.exec() == QMessageBox::Yes) {
            return;
        }
    }
    UgotoviIzbranoLeto();
    QMessageBox msgBox;
    msgBox.setWindowTitle("Izbrišem podate?");
    msgBox.setText(QStringLiteral("Ali želite trajno izbrisati podatke za leto %1?").arg(izbranoLeto));
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, "Da");
    msgBox.setButtonText(QMessageBox::No, "Ne");
    msgBox.setDefaultButton(QMessageBox::Yes);
    if (msgBox.exec() == QMessageBox::Yes) {
        db->exec(QStringLiteral("DELETE FROM Obdavcitev WHERE leto = %1").arg(izbranoLeto));
        PoisciVnesenaDavcnaObdobja();
    }
}

// Poisce vnesena leta v bazi.
void settingsForm::PoisciVnesenaDavcnaObdobja() {
    bool ok;
    widget.comboBoxLeto->clear();
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare("select leto from Obdavcitev");
    qry.exec();
    modal->setQuery(qry);
    // Sesteje st. vnesenih let v bazi.
    stLetVBazi = modal->rowCount();
    int vsaLeta[stLetVBazi];
    // Vnese leta iz baze v polje vsaLeta.
    for (int i = 0; i < stLetVBazi; i++) {
        vsaLeta[i] = modal->record(i).value(0).toInt(&ok);
    }
    // Razvrsti leta.
    std::sort(vsaLeta, vsaLeta+stLetVBazi);
    // Vnese vsa leta v comboBoxLeto.
    for (int i = 0; i < stLetVBazi; i++) {
        widget.comboBoxLeto->addItem(QStringLiteral("%1").arg(vsaLeta[i]));
    }
}

// Vpise ustrezne davcne stopnje v lineEdit-e glede na izbrano leto.
void settingsForm::PoisciVneseneDavcneStopnje() {
    bool ok;
    UgotoviIzbranoLeto();
    QSqlQueryModel * modal = new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(QStringLiteral("select * from Obdavcitev where leto = %1").arg(izbranoLeto));
    qry.exec();
    modal->setQuery(qry);
    for (int i = 0; i < 43; i++) {
        podatkovniBoxi[i]->setText(PretvoriCenteVQString(modal->record(0).value(i + 2).toInt(&ok)));
    }
    // Kadar je na vrsti faktor zmanjsanja, ga ne izpise kot stevilo z dvema decimalnima mestoma.
//    podatkovniBoxi[23]->setText(QStringLiteral("%1").arg(modal->record(0).value(27).toInt(&ok)));
    podatkovniBoxi[25]->setText(PretvoriFaktorVQString(modal->record(0).value(27).toInt(&ok)));
    /*
     popravi ko bo narejena ustrezna funkcija.
     */
    // Izpise neomejeno vrednostim, ki niso omejene.
    widget.lineEditDo5->setText("neomejeno");
    widget.lineEditIzplaciloDo5->setText("neomejeno");
    widget.lineEditOlajsavaDo3->setText("neomejeno");
    PosodobiVrednostiVOnemogoceniLineEdit();
}

// Ugotovi izbrano leto v comboBoxLeto.
void settingsForm::UgotoviIzbranoLeto() {
    izbranoLeto = widget.comboBoxLeto->itemText(widget.comboBoxLeto->currentIndex());
}

// Prevori int v QString. Vrednost izrazena v stotinkah.
QString settingsForm::PretvoriCenteVQString(int i) {
    QString rezultat;
    // Pretvori int v unsigned int, za primer prejema vrednosti "-1".
    unsigned int stevilo = i;
    if (stevilo < 10) {
        rezultat = QStringLiteral("0,0%1").arg(stevilo);
    }
    else if (stevilo < 100) {
        rezultat = QStringLiteral("0,%1").arg(stevilo);
    }
    else {
        rezultat = QStringLiteral("%1,%2").arg(stevilo / 100).arg(stevilo % 100);
        // Poskrbi, da sta za vejico dve stevili.
        if (rezultat.split(",")[1].size() == 1) {
            rezultat = QStringLiteral("%1,0%2").arg(stevilo / 100).arg(stevilo % 100);
        }
    }
    return rezultat;
}

// Prevori int v QString. Vrednost izrazena v 10^(-6).
QString settingsForm::PretvoriFaktorVQString(int i) {
    QString rezultat;
    // Pretvori int v unsigned int, za primer prejema vrednosti "-1".
    unsigned int stevilo = i;
    if (stevilo < 10) {
        rezultat = QStringLiteral("0,00000%1").arg(stevilo);
    }
    else if (stevilo < 100) {
        rezultat = QStringLiteral("0,0000%1").arg(stevilo);
    }
    else if (stevilo < 1000) {
        rezultat = QStringLiteral("0,000%1").arg(stevilo);
    }
    else if (stevilo < 10000) {
        rezultat = QStringLiteral("0,00%1").arg(stevilo);
    }
    else if (stevilo < 100000) {
        rezultat = QStringLiteral("0,0%1").arg(stevilo);
    }
    else if (stevilo < 1000000) {
        rezultat = QStringLiteral("0,%1").arg(stevilo);
    }
    else {
        rezultat = QStringLiteral("%1,%2").arg(stevilo / 1000000).arg(stevilo % 1000000);
        // Poskrbi, da sta za vejico dve stevili.
        if (rezultat.split(",")[1].size() == 1) {
            rezultat = QStringLiteral("%1,0%2").arg(stevilo / 1000000).arg(stevilo % 1000000);
        }
    }
    return rezultat;
}

// Pretvori QString v int in vrne vneseno vrednost v centih.
unsigned int settingsForm::PretvoriQStringVCente(QString vnos) {
    bool ok;
    int skupnoCenti;
    // Izracunaj vneseno vrednost v centih.
    if (vnos.contains(",")) {
        int evriVCente = 100 * vnos.split(",")[0].toInt(&ok);
        int centi;
        // Ugotovi koliko decimalnih mest je za vejico.
        int decimalnaMesta = vnos.split(",")[1].size();
        // Spremenljivki centi dodeli ustrezno vrednost.
        switch (decimalnaMesta) {
            case 1: {
                centi = 10 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 2: {
                centi = vnos.split(",")[1].toInt(&ok);
                break;
            }
            default: {
                centi = 0;
            }
        }
        skupnoCenti = evriVCente + centi;
    } else {
        skupnoCenti = 100 * vnos.toInt(&ok);
    }
    return skupnoCenti;
}

// Pretvori QString v int in vrne vneseno vrednost v 10^6.
unsigned int settingsForm::PretvoriQStringVFaktor(QString vnos) {
    bool ok;
    int skupno;
    // Izracunaj vneseno vrednost v centih.
    if (vnos.contains(",")) {
        int predVejico = 1000000 * vnos.split(",")[0].toInt(&ok);
        int zaVejico;
        // Ugotovi koliko decimalnih mest je za vejico.
        int decimalnaMesta = vnos.split(",")[1].size();
        // Spremenljivki centi dodeli ustrezno vrednost.
        switch (decimalnaMesta) {
            case 1: {
                zaVejico = 100000 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 2: {
                zaVejico = 10000 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 3: {
                zaVejico = 1000 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 4: {
                zaVejico = 100 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 5: {
                zaVejico = 10 * vnos.split(",")[1].toInt(&ok);
                break;
            }
            case 6: {
                zaVejico = vnos.split(",")[1].toInt(&ok);
                break;
            }
            default: {
                zaVejico = 0;
            }
        }
        skupno = predVejico + zaVejico;
    } else {
        skupno = 1000000 * vnos.toInt(&ok);
    }
    return skupno;
}

// Zahteva ustrezen vnos stevil.
void settingsForm::ValidirajVnos() {
    // Vnos za zneske in odstotke.
    QRegExp regExpPlaca("^[1-9]{1}\\d{0,6}(,\\d{0,2}){0,1}$|^0{1},[0-9]{0,2}");
    // Vnos za faktor, na 6 decimalk.
    QRegExp regExpFaktor("^0,\\d{0,6}|^[1-9]{1}\\d{0,2},\\d{0,6}");
    for (int i = 0; i < 43; i++) {
        // Ko je na vrsti faktor uporabi drug regExp.
        if (i != 25) {
            podatkovniBoxi[i]->setValidator(new QRegExpValidator(regExpPlaca, this));
        }
        else {
            podatkovniBoxi[i]->setValidator(new QRegExpValidator(regExpFaktor, this));
        }
    }
}

// Definira PodatkovniBoxi in nekaterim dodeli lastnosti.
void settingsForm::DefinirajPodatkovneBoxe() {
    widget.lineEditOd1->setText("0,00");
    widget.lineEditOd1->setDisabled(true);
    widget.lineEditOd2->setDisabled(true);
    widget.lineEditOd3->setDisabled(true);
    widget.lineEditOd4->setDisabled(true);
    widget.lineEditOd5->setDisabled(true);
    widget.lineEditDo5->setDisabled(true);
    widget.lineEditDavekDoStopnje1->setText("0,00");
    widget.lineEditDavekDoStopnje1->setDisabled(true);
    widget.lineEditDavekDoStopnje2->setDisabled(true);
    widget.lineEditDavekDoStopnje3->setDisabled(true);
    widget.lineEditDavekDoStopnje4->setDisabled(true);
    widget.lineEditDavekDoStopnje5->setDisabled(true);
    widget.lineEditIzplaciloDo1->setDisabled(true);
    widget.lineEditIzplaciloDo2->setDisabled(true);
    widget.lineEditIzplaciloDo3->setDisabled(true);
    widget.lineEditIzplaciloDo4->setDisabled(true);
    widget.lineEditIzplaciloDo5->setDisabled(true);
    widget.lineEditOlajsavaOd1->setText("0,00");
    widget.lineEditOlajsavaOd1->setDisabled(true);
    widget.lineEditOlajsavaOd2->setDisabled(true);
    widget.lineEditOlajsavaDo3->setDisabled(true);
    widget.lineEditOlajsavaDo2->setDisabled(true);
    widget.lineEditOlajsavaZnesek2->setDisabled(true);
    // Stopnje prispevkov.
    podatkovniBoxi[0] = widget.lineEditStopnjaDelojemalec;
    podatkovniBoxi[1] = widget.lineEditStopnjaDelodajalec;    
    // Podatki za dohodnino.
    podatkovniBoxi[2] = widget.lineEditDo1;
    podatkovniBoxi[3] = widget.lineEditDavekDoStopnje1;
    podatkovniBoxi[4] = widget.lineEditStopnja1;
    podatkovniBoxi[5] = widget.lineEditIzplaciloDo1;
    podatkovniBoxi[6] = widget.lineEditDo2;
    podatkovniBoxi[7] = widget.lineEditDavekDoStopnje2;
    podatkovniBoxi[8] = widget.lineEditStopnja2;
    podatkovniBoxi[9] = widget.lineEditIzplaciloDo2;
    podatkovniBoxi[10] = widget.lineEditDo3;
    podatkovniBoxi[11] = widget.lineEditDavekDoStopnje3;
    podatkovniBoxi[12] = widget.lineEditStopnja3;
    podatkovniBoxi[13] = widget.lineEditIzplaciloDo3;
    podatkovniBoxi[14] = widget.lineEditDo4;
    podatkovniBoxi[15] = widget.lineEditDavekDoStopnje4;
    podatkovniBoxi[16] = widget.lineEditStopnja4;
    podatkovniBoxi[17] = widget.lineEditIzplaciloDo4;
    podatkovniBoxi[18] = widget.lineEditDo5;
    podatkovniBoxi[19] = widget.lineEditDavekDoStopnje5;
    podatkovniBoxi[20] = widget.lineEditStopnja5;
    podatkovniBoxi[21] = widget.lineEditIzplaciloDo5;
    // Podatki za splosne olajsave.
    podatkovniBoxi[22] = widget.lineEditOlajsavaDo1;
    podatkovniBoxi[23] = widget.lineEditOlajsavaZnesek1;
    podatkovniBoxi[24] = widget.lineEditOlajsavaOsnovaZaPribitek;
    podatkovniBoxi[25] = widget.lineEditOlajsavaFaktorZmanjsanja;
    podatkovniBoxi[26] = widget.lineEditOlajsavaOd3;
    podatkovniBoxi[27] = widget.lineEditOlajsavaZnesek3;
    // Podatki za olajsave za otroke.
    podatkovniBoxi[28] = widget.lineEditOtrok1;
    podatkovniBoxi[29] = widget.lineEditOtrok2;
    podatkovniBoxi[30] = widget.lineEditOtrok3;
    podatkovniBoxi[31] = widget.lineEditOtrok4;
    podatkovniBoxi[32] = widget.lineEditOtrok5;
    podatkovniBoxi[33] = widget.lineEditOtrokDodatni;
    // Podatki za olajsave za otroke s posebnimi potrebami.
    podatkovniBoxi[34] = widget.lineEditOtrokSPosPot1;
    podatkovniBoxi[35] = widget.lineEditOtrokSPosPot2;
    podatkovniBoxi[36] = widget.lineEditOtrokSPosPot3;
    podatkovniBoxi[37] = widget.lineEditOtrokSPosPot4;
    podatkovniBoxi[38] = widget.lineEditOtrokSPosPot5;
    podatkovniBoxi[39] = widget.lineEditOtrokSPosPotDodatni;
    // Podatki za druge olajsave.
    podatkovniBoxi[40] = widget.lineEditDrugDruzinskiClan;
    podatkovniBoxi[41] = widget.lineEditInvalid;
    podatkovniBoxi[42] = widget.lineEditDijakStudent;
}

// Ob spremembi v nekaterih LineEdit-ih posodobi ustrezne LineEdit-e.
void settingsForm::PosodobiVrednostiVOnemogoceniLineEdit() {
    widget.lineEditOd2->setText(widget.lineEditDo1->text());
    widget.lineEditOd3->setText(widget.lineEditDo2->text());
    widget.lineEditOd4->setText(widget.lineEditDo3->text());
    widget.lineEditOd5->setText(widget.lineEditDo4->text());
    widget.lineEditOlajsavaOd2->setText(widget.lineEditOlajsavaDo1->text());
    widget.lineEditOlajsavaDo2->setText(widget.lineEditOlajsavaOd3->text());
    widget.lineEditOlajsavaZnesek2->setText(widget.lineEditOlajsavaZnesek3->text());
    // Izpiz vrednosti potrebnih za izracune davka do izbrane stopnje.
    unsigned int do1 = PretvoriQStringVCente(widget.lineEditDo1->text());
    unsigned int do2 = PretvoriQStringVCente(widget.lineEditDo2->text());
    unsigned int do3 = PretvoriQStringVCente(widget.lineEditDo3->text());
    unsigned int do4 = PretvoriQStringVCente(widget.lineEditDo4->text());
    unsigned int stopnja1 = PretvoriQStringVCente(widget.lineEditStopnja1->text());
    unsigned int stopnja2 = PretvoriQStringVCente(widget.lineEditStopnja2->text());
    unsigned int stopnja3 = PretvoriQStringVCente(widget.lineEditStopnja3->text());
    unsigned int stopnja4 = PretvoriQStringVCente(widget.lineEditStopnja4->text());
    // Izracuni za davek do stopnje.
    unsigned int davekDo2 = Pomnozi(do1, stopnja1, 4);
    unsigned int davekDo3 = Pomnozi(do2 - do1, stopnja2, 4) + davekDo2;
    unsigned int davekDo4 = Pomnozi(do3 - do2, stopnja3, 4) + davekDo3;
    unsigned int davekDo5 = Pomnozi(do4 - do3, stopnja4, 4) + davekDo4;
    // Izpis davka.
    widget.lineEditDavekDoStopnje2->setText(PretvoriCenteVQString(davekDo2));
    widget.lineEditDavekDoStopnje3->setText(PretvoriCenteVQString(davekDo3));
    widget.lineEditDavekDoStopnje4->setText(PretvoriCenteVQString(davekDo4));
    widget.lineEditDavekDoStopnje5->setText(PretvoriCenteVQString(davekDo5));
}

// Za mnozenje pozitivnih int. Vrne po potrebi za izbran faktor pomanjsano stevilo.
unsigned int settingsForm::Pomnozi(unsigned int faktor1, unsigned int faktor2, short odrezi) {
    unsigned long long longFaktor1 = faktor1;
    unsigned long long longFaktor2 = faktor2;
    unsigned long long longProdukt = longFaktor1 * longFaktor2;
    unsigned int produkt;
    // Testiraj ce ni natanco, naredi funckijo v tem primeru.
    unsigned long long delitelj = pow(10, odrezi);
    // Ce je stevilo preveliko, vrni 0.
    if ((longProdukt / delitelj) >= 4294967295) {
        return 0;
    }
    else {
        produkt = (unsigned int) (longProdukt / delitelj);
    }
    // Ce je ostanek >= 0.5 pristej 1 produktu. Ce je rezultat preveliko stevilo, vrne 0.
    if (((longProdukt % delitelj) * 2) >= delitelj) {
        return (produkt + 1);
    }
    else {
        return produkt;
    }
}

// Deli 2 stevili. Vrne po potrebi za izbran faktor povecano stevilo.
unsigned int settingsForm::Deli(unsigned int deljenec, unsigned int delitelj, short povecaj) {
    if (delitelj == 0) {
        return 0;
    }
    unsigned long long longDeljenec = deljenec;
    unsigned long long longDelitelj = delitelj;
    unsigned long long longFaktor = pow(10, povecaj);
    unsigned long long longKolicnik = longDeljenec * longFaktor / longDelitelj;
    unsigned long long ostanek = longDeljenec * longFaktor % longDelitelj;
    // Zaokrozi ostanek, ce je potrebno.
    if (ostanek * 2 >= delitelj) {
        longKolicnik += 1;
    }
    if (longKolicnik > 4294967295) {
        longKolicnik = 0;
    }
    return (int) longKolicnik;
}

// Odpre okno za shraniti novo leto. Formi za shranjevanje posreduje vnesene podatke.
void settingsForm::ShraniPodatkeVNovoLeto() {
    // Preveri ustreznost podatkov.
    bool podatkiUstrezni = PreveriUstreznostVnesenihPodatkov();
    if (podatkiUstrezni && db->isOpen()) {
        QString vrednosti = VneseneVrednosti();
        saveForm *saveF = new saveForm(db, vrednosti);
        saveF->setModal(true);
        saveF->exec();
        PoisciVnesenaDavcnaObdobja();
    }
}

// Odpre okno za shraniti novo leto. Formi za shranjevanje posreduje vnesene podatke in trenutno leto.
void settingsForm::PosodobiPodatke() {
    // Preveri ustreznost podatkov.
    bool podatkiUstrezni = PreveriUstreznostVnesenihPodatkov();
    if (podatkiUstrezni && db->isOpen()) {
        QString vrednosti = VneseneVrednosti();
        saveForm *updateF = new saveForm(db, vrednosti, izbranoLeto);
        updateF->setModal(true);
        updateF->exec();
        PoisciVnesenaDavcnaObdobja();
    }
}

//
bool settingsForm::PreveriUstreznostVnesenihPodatkov() {
    bool brezNapake = true;
    unsigned int do1 = PretvoriQStringVCente(widget.lineEditDo1->text());
    unsigned int do2 = PretvoriQStringVCente(widget.lineEditDo2->text());
    unsigned int do3 = PretvoriQStringVCente(widget.lineEditDo3->text());
    unsigned int do4 = PretvoriQStringVCente(widget.lineEditDo4->text());
    unsigned int stopnjaolajsavaNizjaDo = PretvoriQStringVCente(widget.lineEditOlajsavaDo1->text());
    unsigned int stopnjaolajsavaVisjaOd = PretvoriQStringVCente(widget.lineEditOlajsavaOd3->text());
    if (do1 > do2) {
        PrikaziObvestilo("Neustrezni podatki v kategoriji \"Davčne stopnje\"", "Znesek do prve stopnje mora biti nizji od zneska do druge stopnje.");
        brezNapake = false;
    }
    if (do2 > do3) {
        PrikaziObvestilo("Neustrezni podatki v kategoriji \"Davčne stopnje\"", "Znesek do druge stopnje mora biti nizji od zneska do tretje stopnje.");
        brezNapake = false;
    }
    if (do3 > do4) {
        PrikaziObvestilo("Neustrezni podatki v kategoriji \"Davčne stopnje\"", "Znesek do tretje stopnje mora biti nizji od zneska do četrte stopnje.");
        brezNapake = false;
    }
    if (stopnjaolajsavaNizjaDo >= stopnjaolajsavaVisjaOd) {
        PrikaziObvestilo("Neustrezni podatki v kategoriji \"Splošna olajšava\"", "Vnešena vrednost olasave nizje osnove mora biti nizja od vrednosti višje osnove.");
        brezNapake = false;
    }
    return brezNapake;
}

//
void settingsForm::PrikaziObvestilo(QString naslov, QString vsebina) {
    QMessageBox zavrnjenaZahteva;
    zavrnjenaZahteva.setWindowTitle(naslov);
    zavrnjenaZahteva.setText(vsebina);
    zavrnjenaZahteva.setStandardButtons(QMessageBox::Yes);
    zavrnjenaZahteva.setButtonText(QMessageBox::Yes, "Nadaljuj");
    zavrnjenaZahteva.setDefaultButton(QMessageBox::Yes);
    if (zavrnjenaZahteva.exec() == QMessageBox::Yes) {
        return;
    }
}

//
QString settingsForm::VneseneVrednosti() {
    QString vrednosti = "";
    for (int i = 0; i < 43; i++) {
        // Za faktor zmnjasanja uporabi drugo pretvorbo.
        if (i == 25) {
            vrednosti += QStringLiteral("%1,").arg(PretvoriQStringVFaktor(podatkovniBoxi[i]->text()));
            continue;
        }
        // Za zadnjim stevilom ne zapisi vejice.
        if (i == 42) {
            vrednosti += QStringLiteral("%1").arg(PretvoriQStringVCente(podatkovniBoxi[i]->text()));
            continue;
        }
        // stopnja_5_do in stopnja_5_izplacilo dodeli vrednost -1.
        if (i == 18 || i == 21) {
            vrednosti += "-1,";
            continue;
        }
        vrednosti += QStringLiteral("%1,").arg(PretvoriQStringVCente(podatkovniBoxi[i]->text()));
    }
    return vrednosti;
}