#ifndef _SETTINGSFORM_H
#define _SETTINGSFORM_H

#include "ui_settingsForm.h"
#include <QtSql>

class settingsForm : public QDialog {
    Q_OBJECT
public:
    settingsForm(QSqlDatabase *);
    virtual ~settingsForm();
private:
    Ui::settingsForm widget;
    QSqlDatabase *db;
    QString izbranoLeto;
    short stLetVBazi;
    std::array<QLineEdit*, 43> podatkovniBoxi;
private slots:
    void IzbrisiLeto();
    void PoisciVnesenaDavcnaObdobja();
    void PoisciVneseneDavcneStopnje();
    void UgotoviIzbranoLeto();
    QString PretvoriCenteVQString(int);
    QString PretvoriFaktorVQString(int);
    unsigned int PretvoriQStringVCente(QString);
    unsigned int PretvoriQStringVFaktor(QString);
    void ValidirajVnos();
    void DefinirajPodatkovneBoxe();
    void PosodobiVrednostiVOnemogoceniLineEdit();
    unsigned int Pomnozi(unsigned int, unsigned int, short);
    unsigned int Deli(unsigned int, unsigned int, short);
    void ShraniPodatkeVNovoLeto();
    void PosodobiPodatke();
    bool PreveriUstreznostVnesenihPodatkov();
    void PrikaziObvestilo(QString, QString);
    QString VneseneVrednosti();
};

#endif /* _SETTINGSFORM_H */
