# QT PLACA

Program za izračun dohodnine.

## Uporaba

Za Windows 10 potrebuješ:
1. NetBeans IDE 8.2
2. MinGW
3. Qt 5.1.1 ali novejši

## Izjava o omejitvi odgovornosti

Program lahko vsebuje napake, za katere avtor ne odgovarja.